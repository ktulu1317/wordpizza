﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public Text pointsText;
    public GameObject settingsObject;
    public GameObject quitObject;
    public Button rateButton;

    void Awake()
    {
        if (!PlayerPrefs.HasKey("FirstLaunch"))
        {
            PlayerPrefs.SetInt("FirstLaunch", 0);
        }
        if (PlayerPrefs.GetInt("IsRate") == 1)
        {
            rateButton.interactable = false;
        }
    }
    
    void Start()
    {
        pointsText.text = PlayerPrefs.GetInt("Points").ToString();
        if (PlayerPrefs.GetInt("FirstLaunch") == 1)
        {
            if (AdsController.instance.IsLaunchScreenAdOn == 1)
            {
                AdsController.instance.ShowInterstitial(interstitialType.interstitialLaunch);
            }
            else if (AdsController.instance.IsLaunchScreenAdOn == 2)
            {
                AdsController.instance.ShowAppLovinInterstitial();
            }
            else if (AdsController.instance.IsLaunchScreenAdOn == 3)
            {
                //FACEBOOK AD SHOW
            }
        }
    }

    public void StartGame()
    {
        NativeReviewRequest.RequestReview();
        if (PlayerPrefs.GetInt("FirstLaunch") == 1)
        {
            if (AdsController.instance.IsLaunchScreenAdOn == 1)
            {
                AdsController.instance.ShowInterstitial(interstitialType.interstitialLaunch);
            }
            else if (AdsController.instance.IsLaunchScreenAdOn == 2)
            {
                AdsController.instance.ShowAppLovinInterstitial();
            }
            else if (AdsController.instance.IsLaunchScreenAdOn == 3)
            {
                //FACEBOOK AD SHOW
            }
        }
        SceneManager.LoadScene(1);
        PlayerPrefs.SetInt("FirstLaunch", 1);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ShowQuitMenu();
        }
    }

    void ShowQuitMenu()
    {
        quitObject.SetActive(true);
    }

    public void HideQuitMenu()
    {
        quitObject.SetActive(false);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void ShowSettings()
    {
        settingsObject.SetActive(true);
    }

    public void HideSettings()
    {
        settingsObject.SetActive(false);
    }

    public void Reset()
    {
        PlayerPrefs.DeleteKey("LastPlayed");
        PlayerPrefs.DeleteKey("LevelsFinished");
        PlayerPrefs.DeleteKey("ExtraWordsLevel");
        PlayerPrefs.DeleteKey("ExtraWordsCurrentLevelCount");
        PlayerPrefs.DeleteKey("Points");
    }

    public void Rate()
    {
        PlayerPrefs.SetInt("IsRate", 1);
        CoinsController.instance.ChangeCoinsValue(25);
        rateButton.interactable = false;
#if UNITY_ANDROID
        Application.OpenURL("market://details?id=" + Application.identifier);
#elif UNITY_IPHONE
 Application.OpenURL("itms-apps://itunes.apple.com/app/id" + 1261846793);
#endif
    }
}
