﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    public AudioSource soundSource;

	void Start ()
    {
        if (PlayerPrefs.GetInt("Sound") == 0)
        {
            soundSource.mute = true;
        }
    }
}
