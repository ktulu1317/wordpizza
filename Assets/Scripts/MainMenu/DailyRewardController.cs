﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using UnityEngine.UI;

public enum RewardTypes { TwentyCoins, ThirtyCoins, FiftyPercentDiscount };

public class DailyRewardController : MonoBehaviour
{
    public GameObject dailyReward;
    public CanvasGroup[] allRewards;
    public CanvasGroup[] allSecretRewardsButtons;
    public GameObject closeButton;
    public static bool isDiscountActive;
    public static float discountTimeLeft;

    int secondsAfterDailyReward;
    int currentNumber;
    int rewardNumber;
    bool readyToChoose;
    float[] startPosition = new float[3];
    CoinsController coinsController;

    void Awake()
    {        
        coinsController = FindObjectOfType<CoinsController>();
        if (!PlayerPrefs.HasKey("LastRewardTime"))
        {
            PlayerPrefs.SetString("LastRewardTime", DateTime.Now.ToBinary().ToString());
        }

        DateTime currentDate = DateTime.Now;
        long temp = Convert.ToInt64(PlayerPrefs.GetString("LastRewardTime"));
        DateTime oldDate = DateTime.FromBinary(temp);
        secondsAfterDailyReward = (int)currentDate.Subtract(oldDate).TotalSeconds;
        if (secondsAfterDailyReward > 86400)
        {
            ShowDailyReward();
        }
        PlayerPrefs.SetInt("Discount", 0);
        if (PlayerPrefs.HasKey("DiscountStartTime"))
        {
            DateTime currentDate1 = DateTime.Now;
            long temp1 = Convert.ToInt64(PlayerPrefs.GetString("DiscountStartTime"));
            DateTime oldDate1 = DateTime.FromBinary(temp);
            int secondsAfterDailyReward1 = (int)currentDate.Subtract(oldDate).TotalSeconds;           
            if (secondsAfterDailyReward1 < 3600f)
            {
                PlayerPrefs.SetInt("Discount", 1);
            }
            else
            {
                PlayerPrefs.SetInt("Discount", 0);
            }
        }
    }

    public void ShowDailyReward()
    {
        dailyReward.SetActive(true);
        Invoke("StartDailyRewardStartAnim", 0.5f);
    }

    void StartDailyRewardStartAnim()
    {
        for (int i = 0; i < allRewards.Length; i++)
        {
            allRewards[i].DOFade(0f, 1f);
            allSecretRewardsButtons[i].DOFade(1f, 1f);
        }
        Invoke("StartMixAnimation", 1f);
    }

    void StartMixAnimation()
    {
        Sequence mySequence = DOTween.Sequence();        
        for (int i = 0; i < allSecretRewardsButtons.Length; i++)
        {
            startPosition[i] = allSecretRewardsButtons[i].gameObject.GetComponent<RectTransform>().localPosition.x;
            mySequence.Join(allSecretRewardsButtons[i].gameObject.GetComponent<RectTransform>().DOLocalMoveX(0f, 0.5f));
        }
        Invoke("ContinueMixAnimation", 1f);
    }

    void ContinueMixAnimation()
    {
        for (int i = 0; i < allSecretRewardsButtons.Length; i++)
        {
            allSecretRewardsButtons[i].gameObject.GetComponent<RectTransform>().DOLocalMoveX(startPosition[2 - i], 0.5f);
        }
        Invoke("MakeReadyToChoose", 0.5f);
    }

    void MakeReadyToChoose()
    {
        readyToChoose = true;
    }

    public void ChooseDailyReward(int number)
    {
        if (readyToChoose)
        {
            currentNumber = number;
            rewardNumber = UnityEngine.Random.Range(0, 3);
            for (int i = 0; i < allSecretRewardsButtons.Length; i++)
            {
                if (number != i)
                {
                    allSecretRewardsButtons[i].DOFade(0f, 1f);
                }
            }
            Invoke("MakeButtonAtCenter", 0.7f);
        }
    }

    void MakeButtonAtCenter()
    {
        allSecretRewardsButtons[currentNumber].gameObject.GetComponent<RectTransform>().DOLocalMoveX(0f, 0.5f);
        allRewards[rewardNumber].gameObject.GetComponent<RectTransform>().DOLocalMoveX(0f, 0.5f);
        Invoke("ShowReward", 0.6f);
    }

    void ShowReward()
    {
        allRewards[rewardNumber].DOFade(1f, 1f);
        allSecretRewardsButtons[currentNumber].DOFade(0f, 1f);
        Invoke("ShowCloseButton", 1f);
    }

    void ShowCloseButton()
    {
        closeButton.SetActive(true);
    }

    public void CloseDailyReward()
    {
        closeButton.SetActive(false);
        dailyReward.SetActive(false);
        readyToChoose = false;
        for (int i = 0; i < 3; i++)
        {
            allSecretRewardsButtons[i].gameObject.GetComponent<RectTransform>().DOLocalMoveX(startPosition[i], 0f);
            allRewards[i].gameObject.GetComponent<RectTransform>().DOLocalMoveX(startPosition[i], 0f);
            allRewards[i].DOFade(1f, 0f);
        }
        GetReward();

    }

    void GetReward()
    {
        if (rewardNumber == (int)RewardTypes.TwentyCoins)
        {
            coinsController.ChangeCoinsValue(20);
        }
        else if (rewardNumber == (int)RewardTypes.ThirtyCoins)
        {
            coinsController.ChangeCoinsValue(30);
        }
        else if (rewardNumber == (int)RewardTypes.FiftyPercentDiscount)
        {
            PlayerPrefs.SetInt("Discount", 1);
            PlayerPrefs.SetString("DiscountStartTime", DateTime.Now.ToBinary().ToString());
            long temp = Convert.ToInt64(PlayerPrefs.GetString("DiscountStartTime"));
            DateTime oldDate = DateTime.FromBinary(temp);
            discountTimeLeft = 3600f;
        }
        PlayerPrefs.SetString("LastRewardTime", DateTime.Now.ToBinary().ToString());
    }
}
