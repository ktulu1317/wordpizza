﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AudioMainController : MonoBehaviour
{
    public Image backgroundMusicButtonImage;
    public Image soundButtonImage;
    public AudioSource soundSource;

    AudioSource backgroundMusic;

    void Start()
    {
        backgroundMusic = GameObject.FindGameObjectWithTag("BackgroundAudio").GetComponent<AudioSource>();

        if (PlayerPrefs.GetInt("BackgroundMusic") == 0)
        {
            backgroundMusicButtonImage.color = new Color(1f, 1f, 1f, 0.5f);
            backgroundMusic.mute = true;
        }

        if (PlayerPrefs.GetInt("Sound") == 0)
        {
            soundButtonImage.color = new Color(1f, 1f, 1f, 0.5f);
            soundSource.mute = true;
        }
    }

    public void ChangeBackgroundMusicState()
    {
        if (!backgroundMusic)
        {
            backgroundMusic = GameObject.FindGameObjectWithTag("BackgroundAudio").GetComponent<AudioSource>();
        }
        if (PlayerPrefs.GetInt("BackgroundMusic") == 1)
        {
            PlayerPrefs.SetInt("BackgroundMusic", 0);
            backgroundMusicButtonImage.color = new Color(1f, 1f, 1f, 0.5f);
            backgroundMusic.mute = true;
        }
        else
        {
            PlayerPrefs.SetInt("BackgroundMusic", 1);
            backgroundMusicButtonImage.color = new Color(1f, 1f, 1f, 1f);
            backgroundMusic.mute = false;
        }
    }

    public void ChangeSoundState()
    {
        if (PlayerPrefs.GetInt("Sound") == 1)
        {
            PlayerPrefs.SetInt("Sound", 0);
            soundButtonImage.color = new Color(1f, 1f, 1f, 0.5f);
            soundSource.mute = true;
        }
        else
        {
            PlayerPrefs.SetInt("Sound", 1);
            soundButtonImage.color = new Color(1f, 1f, 1f, 1f);
            soundSource.mute = false;
        }
    }
}
