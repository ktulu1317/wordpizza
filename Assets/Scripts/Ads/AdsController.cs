﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;
using UnityEngine.SceneManagement;

public enum interstitialType { interstitialLaunch, interstitialMinimize, interstitialRefuseInApp, interstitialShare };

public class AdsController : MonoBehaviour
{
    public static AdsController instance;
    public bool showOnFocus = true;

    // first ID for Android, second for iOS
    private static string[] bannerID = { "ca-app-pub-2723604601060865/6360772035", "ca-app-pub-5077207467736717/5723257095" };
    private static string[] interstitialLaunchID = { "ca-app-pub-2723604601060865/9314238436", "ca-app-pub-5077207467736717/1758648278" };
    private static string[] interstitialMinimizeID = { "ca-app-pub-2723604601060865/9174637639", "ca-app-pub-5077207467736717/1100172723" };
    private static string[] interstitialRefuseInAppID = { "ca-app-pub-2723604601060865/3128104030", "ca-app-pub-5077207467736717/7499373195" };
    private static string[] interstitialShareID = { "ca-app-pub-2723604601060865/3267704835", "ca-app-pub-2723604601060865/3267704835" };

    private BannerView banner;
    private InterstitialAd interstitialLaunchAd; // on Play, Quit, Next
    private InterstitialAd interstitialMinimizeAd;
    private InterstitialAd interstitialRefuseInAppAd;
    private InterstitialAd interstitialShareAd;

    private bool wasMinimized = false;
    public bool showShopAd = false;
    public bool isShopShown = false;
    public int adsForSkipCounter = 0;
    public bool skipNextMinimize = false;

    private int adType;
    private int bannerType;
    private int cancelScreenAdType;

    private int inAppCloseAdOn;
    private int isShareScreenAdOn;
    private int isLaunchScreenAdOn;
    private int isBackgroundScreenAdOn;

    private int inAppCloseCount;

    private float timeToNextAd = 0;
    private float maxTimeToNextAd = 120;

    static bool isLoad;

    private Scene lastScene;

    ConfigLoader configLoader;

    public int InAppCloseAdOn
    {
        get
        {
            return inAppCloseAdOn;
        }

        set
        {
            inAppCloseAdOn = value;
        }
    }

    public int IsShareScreenAdOn
    {
        get
        {
            return isShareScreenAdOn;
        }

        set
        {
            isShareScreenAdOn = value;
        }
    }

    public int IsLaunchScreenAdOn
    {
        get
        {
            return isLaunchScreenAdOn;
        }

        set
        {
            isLaunchScreenAdOn = value;
        }
    }

    public int IsBackgroundScreenAdOn
    {
        get
        {
            return isBackgroundScreenAdOn;
        }

        set
        {
            isBackgroundScreenAdOn = value;
        }
    }

    public int AdType
    {
        get
        {
            return adType;
        }

        set
        {
            adType = value;
        }
    }

    public int BannerType
    {
        get
        {
            return bannerType;
        }

        set
        {
            bannerType = value;
        }
    }

    public int CancelScreenAdType
    {
        get
        {
            return cancelScreenAdType;
        }

        set
        {
            cancelScreenAdType = value;
        }
    }

    public int InAppCloseCount
    {
        get
        {
            return inAppCloseCount;
        }

        set
        {
            inAppCloseCount = value;
        }
    }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;

            if (!PlayerPrefs.HasKey("NoAds"))
            {
                PlayerPrefs.SetInt("NoAds", 0);
            }

            DontDestroyOnLoad(this.gameObject);
        }
    }

    void Update()
    {
        if (timeToNextAd > 0)
        {
            timeToNextAd -= Time.deltaTime;
        }
        //Debug.Log(timeToNextAd);
    }

    public void ChangeIsLoad()
    {
        isLoad = true;
    }

    public bool GetIsLoad()
    {
        return isLoad;
    }

    public void ChangeAdmobKeys()
    {
        configLoader = FindObjectOfType<ConfigLoader>();
        bannerID[0] = configLoader.ReturnAdmob().bannerKey;
        bannerID[1] = configLoader.ReturnAdmob().bannerKey;

        interstitialLaunchID[0] = configLoader.ReturnAdmob().interstitialKey;
        interstitialLaunchID[1] = configLoader.ReturnAdmob().interstitialKey;

        interstitialMinimizeID[0] = configLoader.ReturnAdmob().interstitialKey;
        interstitialMinimizeID[1] = configLoader.ReturnAdmob().interstitialKey;

        interstitialRefuseInAppID[0] = configLoader.ReturnAdmob().interstitialKeyCancel;
        interstitialRefuseInAppID[1] = configLoader.ReturnAdmob().interstitialKeyCancel;

        interstitialShareID[0] = configLoader.ReturnAdmob().interstitialKeyShare;
        interstitialShareID[1] = configLoader.ReturnAdmob().interstitialKeyShare;

        //LoadAllAd();
    }

    public void LoadAllAd()
    {
        if (AdsController.instance.isAdAllowed())
        {
            interstitialLaunchAd = LoadAd(interstitialLaunchID);
            interstitialMinimizeAd = LoadAd(interstitialMinimizeID);
            interstitialRefuseInAppAd = LoadAd(interstitialRefuseInAppID);
            //interstitialShareAd = LoadAd(interstitialShareID);
            ShowBanner();
        }
    }

    public void GetAdManage()
    {
        adType = configLoader.ReturnManageData().adType;
        bannerType = configLoader.ReturnManageData().bannerType;
        cancelScreenAdType = configLoader.ReturnManageData().cancelScreenAdType;

        inAppCloseAdOn = configLoader.ReturnManageData().inAppCloseAdOn;
        isShareScreenAdOn = configLoader.ReturnManageData().isShareScreenAdOn;
        isLaunchScreenAdOn = configLoader.ReturnManageData().isLaunchScreenAdOn;
        isBackgroundScreenAdOn = configLoader.ReturnManageData().isBackgroundScreenAdOn;

        inAppCloseCount = configLoader.ReturnManageData().inAppCloseCount;
    }


    public void DisableAds()
    {
        PlayerPrefs.SetInt("NoAds", 1);
        HideBanner();
        CoinsController.instance.DisableNoAdsButton();
    }

    private bool isAdAllowed()
    {
        // return false if noAds purchased 
        bool isAllowed = true;
        if (PlayerPrefs.GetInt("NoAds") == 1)
        {
            isAllowed = false;
        }
        return isAllowed;
    }

    void OnApplicationFocus(bool hasFocus)
    {
        Debug.Log("=======================");
        Debug.Log("hasFocus = " + hasFocus);
        Debug.Log("adsForSkipCounter = " + adsForSkipCounter);
        Debug.Log("AdsController.instance.isShopShown = " + AdsController.instance.isShopShown);

        // handler for handling maximize/minimize events
        if (hasFocus)
        {
            if ((lastScene == SceneManager.GetActiveScene()))
            {
                if (adsForSkipCounter > 0)
                {
                    adsForSkipCounter--;
                }
                else
                {
                    if ((AdsController.instance.isShopShown == false) && (wasMinimized) && showOnFocus)
                    {
                        if (isBackgroundScreenAdOn == 1)
                        {
                            AdsController.instance.ShowInterstitial(interstitialType.interstitialMinimize);
                        }
                        else if (isBackgroundScreenAdOn == 2)
                        {
                            if (AppLovin.HasPreloadedInterstitial())
                            {
                                ShowAppLovinInterstitial();
                            }
                        }
                        else if (isBackgroundScreenAdOn == 3)
                        {
                            //FACEBOOK AD SHOW                            
                        }

                    }
                }
            }
            else {
                adsForSkipCounter = 0;
            }
        }
        else {
            lastScene = SceneManager.GetActiveScene();
            wasMinimized = true;
        }
        //showOnFocus = true;
    }

    private InterstitialAd LoadAd(string[] IDs)
    {
        int platformID = 0;
#if UNITY_IPHONE
                                    platformID = 1;
#endif
        InterstitialAd interstitial = new InterstitialAd(IDs[platformID]);
        AdRequest request = new AdRequest.Builder().Build();
        interstitial.LoadAd(request);
        interstitial.OnAdClosed += HandleOnAdClosed;
        return interstitial;
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        if (AdsController.instance.isAdAllowed())
        {
            Debug.Log("HandleOnAdClosed");
            if (sender == interstitialLaunchAd)
            {
                interstitialLaunchAd = LoadAd(interstitialLaunchID);
            }
            if (sender == interstitialMinimizeAd)
            {
                interstitialMinimizeAd = LoadAd(interstitialMinimizeID);
            }
            if (sender == interstitialRefuseInAppAd)
            {
                interstitialRefuseInAppAd = LoadAd(interstitialRefuseInAppID);
            }
            wasMinimized = false;
        }
    }

    public void ShowInterstitial(interstitialType _interstitialType)
    {
        if (AdsController.instance.isAdAllowed())
        {
            InterstitialAd interstitial = null;
            switch (_interstitialType)
            {
                case interstitialType.interstitialLaunch:
                    interstitial = interstitialLaunchAd;
                    break;
                case interstitialType.interstitialMinimize:
                    interstitial = interstitialMinimizeAd;
                    break;
                case interstitialType.interstitialRefuseInApp:
                    interstitial = interstitialRefuseInAppAd;
                    break;
                case interstitialType.interstitialShare:
                    interstitial = interstitialShareAd;
                    break;
            }
            if (interstitial != null)
            {
                if (interstitial.IsLoaded())
                {
                    adsForSkipCounter = 1;
                    Debug.Log("Interstitial is loaded");
                    if (interstitial == interstitialLaunchAd)
                    {
                        if (timeToNextAd <= 0)
                        {
                            timeToNextAd = maxTimeToNextAd;
                            interstitial.Show();
                        }
                    }
                    else {
                        interstitial.Show();
                    }

                }
                else {
                    Debug.Log("Interstitial is NOT loaded");
                }

            }
            else {
                Debug.Log("Interstitial is null");
            }
        }
    }

    public void ShowAppLovinInterstitial()
    {
        if (isAdAllowed())
        { 
            adsForSkipCounter = 1;
            if (AppLovin.HasPreloadedInterstitial())
            {
                AppLovin.ShowInterstitial();
            }
        }
    }

    public void ShowBanner()
    {
        int platformID = 0;
#if UNITY_IPHONE
            platformID = 1;
#endif
        if (bannerType == 1)
        {
            banner = new BannerView(bannerID[platformID], AdSize.Banner, AdPosition.Bottom);
            AdRequest request = new AdRequest.Builder().Build();
            banner.LoadAd(request);
        }
        else if (bannerType == 2)
        {
            AppLovin.ShowAd(AppLovin.AD_POSITION_CENTER, AppLovin.AD_POSITION_BOTTOM);
        }
        else if (bannerType == 3)
        {
            // FACEBOOK BANNER SHOW  
        }

    }

    public void RevealBanner()
    {
        Debug.Log("RevealBanner");
        if (bannerType == 1)
        {
            if ((banner != null) && (isAdAllowed()))
            {
                banner.Show();
            }
        }
        else if (bannerType == 2)
        {
            if(isAdAllowed())
            {
                AppLovin.ShowAd(AppLovin.AD_POSITION_CENTER, AppLovin.AD_POSITION_BOTTOM);
            }            
        }
        else if (bannerType == 3)
        {
            // FACEBOOK BANNER SHOW
        }
    }

    public void HideBanner()
    {
        Debug.Log("HideBanner");

        if (bannerType == 1)
        {
            Debug.Log("HideBanner1");
            if (banner != null)
            {
                Debug.Log("HideBanner2");
                banner.Hide();
            }
        }
        else if (bannerType == 2)
        {
            AppLovin.HideAd();
        }
        else if (bannerType == 3)
        {
            // FACEBOOK BANNER HIDE
        }
    }

    public void WaitFocus()
    {
        Invoke("ChangeFocus", 30f);
    }

    void ChangeFocus()
    {
        showOnFocus = true;
    }
}
