﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WyrmTale;

public class ConfigLoader : MonoBehaviour
{
    protected static ConfigLoader _instance;
    public static ConfigLoader instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ConfigLoader>();
                if (_instance == null)
                {
                    _instance = new GameObject("Config").AddComponent<ConfigLoader>();
                }
            }
            return _instance;
        }
    }

    public delegate void ConfigLoadedDelegate();
    protected ConfigLoadedDelegate onLoadedCallback;
    public static ConfigLoadedDelegate loadCallback
    {
        get
        {
            return instance.onLoadedCallback;
        }

        set
        {
            instance.onLoadedCallback = value;
        }
    }


    #region Data from server

    protected RatePopupData msgData;
    protected ManagingAdData manageData;
    protected AdmobData adsData;
    protected FacebookData fbData;

    public static RatePopupData ratePopupData
    {
        get
        {
            return instance.msgData;
        }
    }

    public static AdmobData admobData
    {
        get
        {
            return instance.adsData;
        }
    }

    public static ManagingAdData ManageData
    {
        get
        {
            return instance.manageData;
        }
    }

    public static FacebookData FbData
    {
        get
        {
            return instance.fbData;
        }
    }

    public AdmobData ReturnAdmob()
    {
        return admobData;
    }

    public FacebookData ReturnFacebook()
    {
        return FbData;
    }

    public ManagingAdData ReturnManageData()
    {
        return manageData;
    }

    protected void DownloadMessageInfo(float delay = 0)
    {
        StartCoroutine(DowloadMessageInfoCoroutine(delay));
    }

    protected IEnumerator DowloadMessageInfoCoroutine(float delay)
    {
        yield return new WaitForSeconds(delay);


#if UNITY_ANDROID
		WWW www = new WWW("http://appassets.theappauthors.com/catalog/web/api/getAdsSettings?appName=WordPizzaGame&deviceType=301");
#elif UNITY_IPHONE
        WWW www = new WWW("http://appassets.theappauthors.com/catalog/web/api/getAdsSettings?appName=WordPizzaGameiOS&deviceType=301");
        #endif

		//www = new WWW("http://appassets.theappauthors.com/catalog/web/api/getAdsSettings?appName=WordPizzaGameiOS&deviceType=301");



        yield return www;

        if (www.error == null)
        {
            Debug.Log(www.text);

            JSON messageData = new JSON();
            messageData.serialized = www.text;

            msgData = new RatePopupData(messageData.ToArray<JSON>("FanTurningOffOn")[0]);
            manageData = new ManagingAdData(messageData.ToArray<JSON>("ManagingAd")[0]);
            adsData = new AdmobData(messageData.ToArray<JSON>("AdmobKey")[0]);
            fbData = new FacebookData(messageData.ToArray<JSON>("FacebookKey")[0]);
            if (!AdsController.instance.GetIsLoad())
            {
                AdsController.instance.ChangeAdmobKeys();
                AdsController.instance.GetAdManage();
                AdsController.instance.LoadAllAd();
                AdsController.instance.ChangeIsLoad();
            }


            if(onLoadedCallback != null)
            {
                onLoadedCallback.Invoke();
            }
        }
        else
        {
            DownloadMessageInfo(delay + 1.5f);
        }
    }

    void Awake()
    {
        DownloadMessageInfo();
        adsData = new AdmobData();
    }


    // {
    // "AdmobKey":[{"Banner":"","InterstitialLaunch":"","InterstitialShare":"","InterstitialCancel":""}],
    // "FacebookKey":[{"Banner":"","InterstitialLaunch":"","InterstitialShare":"","InterstitialCancel":""}],
    // "AppLovinKey":[{"Banner":"","InterstitialLaunch":"","InterstitialShare":"","InterstitialCancel":""}],
    // "AppoDeal":[{"Banner":"","InterstitialLaunch":"","InterstitialShare":"","InterstitialCancel":""}],
    // "ManagingAd":[{"AdType":"1","BannerType":"0","InAppCloseAdOn":"1","CancelScreenAdType":"1","InAppCloseCount":"0","IsShareScreenAdOn":"1","IsLaunchScreenAdOn":"1","IsBackgroundScreenAdOn":"1"}],
    // "FanTurningOffOn":[{"FanType":"2","FanTitle":"NEED YOUR HELP","FanMessage":"This app was made by a young girl who is trying to feed her family of six people through the income generated from this app. Please rate and review this app. A positive review will give me motivation to working more on the app and making its UX better. It will only take just a few seconds of your time. PLEASE HELP!","EmotionTitle":"NEED YOUR HELP","EmotionMessage":"This app was made by a young girl who is trying to feed her family of six people through the income generated from this app. Please rate and review this app. A positive review will give me motivation to working more on the app and making its UX better. It will only take just a few seconds of your time. PLEASE HELP!","TyporamaTitle":"title 3","TyporamaMessage":"message 3"}],
    // "ManagingAppCrash":[{"crashOnShare":"0"}],
    // "ManagingStorePage":[{"StorePageName":"Sales Page 1","StorePageOption":"1"}],"GoatInBox":[]
    //}

    public class AdmobData
    {
        protected string bannerUnitId;
        protected string interstitialUnitId;
        protected string interstitialShareId;
        protected string interstitialCancelId;
#if UNITY_ANDROID
        string defaultBannerAdId = "ca-app-pub-5077207467736717/2740557455";
        string defaultInterstitialUnitId = "ca-app-pub-5077207467736717/1635359340";
        string defaultInterstitialShareId = "ca-app-pub-5077207467736717/9087130498";
        string defaultInterstitialCancelId = "ca-app-pub-5077207467736717/8719642441";
#elif UNITY_IOS
        string defaultBannerAdId = "ca-app-pub-5077207467736717/5723257095";
        string defaultInterstitialUnitId = "ca-app-pub-5077207467736717/1758648278";
        string defaultInterstitialShareId = "ca-app-pub-5077207467736717/1100172723";
        string defaultInterstitialCancelId = "ca-app-pub-5077207467736717/7499373195";
#endif

        public AdmobData()
        {
            bannerUnitId = defaultBannerAdId;
            interstitialUnitId = defaultInterstitialUnitId;
            interstitialShareId = defaultInterstitialShareId;
            interstitialCancelId = defaultInterstitialCancelId;
        }

		public AdmobData(JSON json)
        {
			bannerUnitId = defaultBannerAdId;
			interstitialUnitId = defaultInterstitialUnitId;
            interstitialShareId = defaultInterstitialShareId;
            interstitialCancelId = defaultInterstitialCancelId;

            string banner = json.ToString("Banner");
            banner = Crypt.instance.Decrypt(banner);
			if(banner.Contains("ca-app-pub"))
            {
				bannerUnitId = banner;
			}
            
			string interstitial = json.ToString("InterstitialLaunch");
            interstitial = Crypt.instance.Decrypt(interstitial);
            if (interstitial.Contains("ca-app-pub"))
            {
				interstitialUnitId = interstitial;
			}

            string interstitialShare = json.ToString("InterstitialShare");
            interstitialShare = Crypt.instance.Decrypt(interstitialShare);
            if (interstitialShare.Contains("ca-app-pub"))
            {
                interstitialShareId = interstitialShare;
            }

            string interstitialCancel = json.ToString("InterstitialCancel");
            interstitialCancel = Crypt.instance.Decrypt(interstitialCancel);
            if (interstitialCancel.Contains("ca-app-pub"))
            {
                interstitialCancelId = interstitialCancel;
            }
        }

        public string bannerKey
        {
            get
            {
                return bannerUnitId;
            }
        }

        public string interstitialKey
        {
            get
            {
                return interstitialUnitId;
            }
        }

        public string interstitialKeyShare
        {
            get
            {
                return interstitialShareId;
            }
        }

        public string interstitialKeyCancel
        {
            get
            {
                return interstitialCancelId;
            }
        }


    }

    public class ManagingAdData
    {
        public int adType;
        public int bannerType;
        public int cancelScreenAdType;

        public int inAppCloseAdOn;
        public int isShareScreenAdOn;
        public int isLaunchScreenAdOn;
        public int isBackgroundScreenAdOn;

        public int inAppCloseCount;

        public ManagingAdData (JSON json)
        {
            adType = json.ToInt("AdType");
            bannerType = json.ToInt("BannerType");
            cancelScreenAdType = json.ToInt("CancelScreenAdType");

            inAppCloseAdOn = json.ToInt("InAppCloseAdOn");
            isShareScreenAdOn = json.ToInt("IsShareScreenAdOn");
            isLaunchScreenAdOn = json.ToInt("IsLaunchScreenAdOn");
            isBackgroundScreenAdOn = json.ToInt("IsBackgroundScreenAdOn");

            inAppCloseCount = json.ToInt("InAppCloseCount");
        }
    }

    public class FacebookData
    {
        protected string bannerUnitId;
        protected string interstitialUnitId;
        protected string interstitialShareId;
        protected string interstitialCancelId;

        string defaultBannerAdId = "ca-app-pub-2723604601060865/6360772035";
        string defaultInterstitialUnitId = "ca-app-pub-2723604601060865/9314238436";
        string defaultInterstitialShareId = "ca-app-pub-2723604601060865/3267704835";
        string defaultInterstitialCancelId = "ca-app-pub-2723604601060865/3128104030";

        public FacebookData(JSON json)
        {
			Debug.Log("json = " + json); 

            bannerUnitId = defaultBannerAdId;
            interstitialUnitId = defaultInterstitialUnitId;
            interstitialShareId = defaultInterstitialShareId;
            interstitialCancelId = defaultInterstitialCancelId;

            string banner = json.ToString("Banner");

			if (banner.Length > 0){
				banner = Crypt.instance.Decrypt(banner);
				if (banner != null)
				{
					bannerUnitId = banner;
					Debug.Log(banner);
				}

				string interstitial = json.ToString("InterstitialLaunch");
				interstitial = Crypt.instance.Decrypt(interstitial);
				if (interstitial != null)
				{
					interstitialUnitId = interstitial;
				}

				string interstitialShare = json.ToString("InterstitialShare");
				interstitialShare = Crypt.instance.Decrypt(interstitialShare);
				if (interstitialShare != null)
				{
					interstitialShareId = interstitialShare;
				}

				string interstitialCancel = json.ToString("InterstitialCancel");
				interstitialCancel = Crypt.instance.Decrypt(interstitialCancel);
				if (interstitialCancel != null)
				{
					interstitialCancelId = interstitialCancel;
				}
			}
        }
    }


    public class RatePopupData
    {
        public int fanType;
        protected string title1;
        protected string message1;

        protected string title2;
        protected string message2;

        protected string title3;
        protected string message3;

        protected string title4;
        protected string message4;

        public RatePopupData(JSON json)
        {
            fanType = json.ToInt("FanType");
            GameController.popUpNumber = fanType;

            title1 = json.ToString("FanTitle");
            title2 = json.ToString("EmotionTitle");
            title3 = json.ToString("TyporamaTitle");
            title4 = json.ToString("AppleReviewTitle");

            message1 = json.ToString("FanMessage");
            message2 = json.ToString("EmotionMessage");
            message3 = json.ToString("TyporamaMessage");
            message4 = json.ToString("AppleReviewMessage");
        }

        public string title
        {
            get
            {
                switch (fanType)
                {
                    case 0:
                        return "";
                    case 1:
                        return title1;
                    case 2:
                        return title2;
                    case 3:
                        return title3;
                    case 4:
                        return title4;
                    default:
                        return title1;
                }
            }
        }

        public string message
        {
            get
            {
                switch (fanType)
                {
                    case 0:
                        return "";
                    case 1:
                        return message1;
                    case 2:
                        return message2;
                    case 3:
                        return message3;
                    case 4:
                        return message4;
                    default:
                        return message1;
                }
            }
        }

        public bool shouldShow
        {
            get
            {
                return fanType > 0;
            }
        }
    }
#endregion
}
