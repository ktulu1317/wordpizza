﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplovinController : MonoBehaviour
{
    private static ApplovinController instance = null;

    public static ApplovinController Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        AppLovin.SetSdkKey("vkCcrdWQSpLV3IMpZMlVrWOu4ohOe1DXmP9b2g0UTM-XeLKDh4iXa5Qeu62X8F4ufDThuTBMrAuTVDhCHPksU8");
        AppLovin.InitializeSdk();
        AppLovin.PreloadInterstitial();
        AppLovin.SetUnityAdListener("ApplovinController");
    }

    void onAppLovinEventReceived(string ev)
    {
        if (ev.Contains("HIDDENINTER"))
        {
            AppLovin.PreloadInterstitial();
        }
    }
}
