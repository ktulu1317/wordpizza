﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RatePopup : MonoBehaviour {

    protected static RatePopup _instance;
    protected static RatePopup instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<RatePopup>();
                if (_instance == null)
                {
                    _instance = new GameObject("Rate").AddComponent<RatePopup>();
                }
            }
            return _instance;
        }
    }


    #region popup state

    protected enum PopupState
    {
        Unknown,
        AlwaysShow,
        DontShowThisLaunch,
        NeverShow,
    }

    protected PopupState _currentState;
    protected PopupState currentState {
        get
        {
            if (_currentState == PopupState.Unknown)
            {
                _currentState = (PopupState)PlayerPrefs.GetInt("show_rate");
                if (_currentState == PopupState.Unknown)
                {
                    _currentState = PopupState.AlwaysShow;
                }
            }
            return _currentState;
        }

        set
        {
            _currentState = value;
            if (_currentState == PopupState.AlwaysShow || _currentState == PopupState.NeverShow)
            {
                PlayerPrefs.SetInt("show_rate", (int)_currentState);
            }
        }
    }

    #endregion


    #region popup window

    protected ConfigLoader.RatePopupData msgData;

    public static bool ShowPopup()
    {
        Debug.Log("SHOW");
        Debug.Log(instance.currentState);
        Debug.Log(instance.msgData);
        AdsController.instance.showOnFocus = false;
        if (instance.currentState == PopupState.AlwaysShow && instance.msgData != null && instance.msgData.shouldShow)
        {
            MNRateUsPopup rateUs = new MNRateUsPopup(instance.msgData.title, instance.msgData.message, "Rate Us", "Later");
            rateUs.SetAppleId(RateUrl);
            rateUs.SetAndroidAppUrl(RateUrl);
            //rateUs.AddDeclineListener(instance.OnNeverPressed);
            rateUs.AddRemindListener(instance.OnLaterPressed);
            rateUs.AddRateUsListener(instance.OnRatePressed);
            rateUs.AddDismissListener(instance.OnLaterPressed);
            rateUs.Show();

            return true;
        }
        return false;
    }


#if UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 
#define PRE_UNITY_5_6
#endif

	protected static string bundleId
    {
        get
        {
#if PRE_UNITY_5_6
    return Application.bundleIdentifier;
#else
    return Application.identifier;
#endif
        }
    }

    protected void OnRatePressed()
    {
		Debug.Log ("RATE");
        currentState = PopupState.NeverShow;
        CoinsController.instance.ChangeCoinsValue(25);
        PlayerPrefs.SetInt("IsRate", 1);
        AdsController.instance.WaitFocus();
    }

	protected static string RateUrl {
		get {
			string url = "";
			if (Application.platform == RuntimePlatform.Android) {
				url = "http://play.google.com/store/apps/details?id=" + bundleId;
			} else {
				if (Application.platform == RuntimePlatform.IPhonePlayer) {
					url = "itms-apps://itunes.apple.com/app/id" + 1261846793;
				}
			}
			return url;
		}
	}

    protected void OnLaterPressed()
    {
		Debug.Log ("LATER");
        currentState = PopupState.DontShowThisLaunch;
        if (msgData.fanType == 1)
        {
            //GameController.Instance.GoToMenuShittyUserDontWantRate();
            currentState = PopupState.AlwaysShow;
        }
        else
        {
            currentState = PopupState.DontShowThisLaunch;
            SceneManager.LoadScene(3);
        }
    }

    protected void OnNeverPressed()
    {
		Debug.Log ("NEVER");        
        if (msgData.fanType == 1)
        {
            //GameController.Instance.GoToMenuShittyUserDontWantRate();
            currentState = PopupState.AlwaysShow;
        }
        else
        {
            currentState = PopupState.NeverShow;
        }
    }

    #endregion

    void Awake()
    {
        ConfigLoader.loadCallback += (() => msgData = ConfigLoader.ratePopupData);
    }
}
