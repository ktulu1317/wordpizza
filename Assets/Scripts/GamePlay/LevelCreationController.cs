﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WyrmTale;
using System.IO;
using UnityEngine.SceneManagement;

public class AnswerContainer
{
    public int id;
    public string word;
    public string definintion;
    public int type;
    public int additionalStars;
    public bool isGuessed;
}

public class LevelCreationController : MonoBehaviour
{
    int id;
    bool isSpecial;
    string letters;
    List<JSON> allAnswersJSON = new List<JSON>();
    List<AnswerContainer> allAnswers = new List<AnswerContainer>();
    public GameObject[] allLettersOnFieldPrefabs;
    bool isLoadLastLevel;

    AnswerFieldController answerFieldController;
    GameController gameController;
    BezierConnection bezierConnection;
    GuessedWordsController guessedWordsController;
    UIController uiController;
    GameObject bezierLetters;
    GameObject[] allLetters;
    JSON level;

    public GameObject[] AllLetters
    {
        get { return allLetters; }
        set { allLetters = value; }
    }

    public List<AnswerContainer> AllAnswers
    {
        get { return allAnswers; }
        set { allAnswers = value; }
    }

    void Start()
    {
        answerFieldController = FindObjectOfType<AnswerFieldController>();
        gameController = FindObjectOfType<GameController>();
        bezierConnection = FindObjectOfType<BezierConnection>();
        guessedWordsController = FindObjectOfType<GuessedWordsController>();
        uiController = FindObjectOfType<UIController>();
        LoadLevelFromFile(GameController.levelNumber);
    }

    void LoadLevelFromFile(int levelNumber)
    {
        if (levelNumber > gameController.levelsCount)
        {
            SceneManager.LoadScene(0);
        }
        else
        {
            TextAsset levelFile = Resources.Load("Level" + levelNumber) as TextAsset;
            level = new JSON();
            level.serialized = levelFile.ToString();
            id = level.ToInt("id");
            isSpecial = level.ToBoolean("isSpecial");
            letters = level.ToString("letters");
            for (int i = 0; i < level.ToArray<JSON>("words").Length; i++)
            {
                allAnswersJSON.Add(level.ToArray<JSON>("words")[i]);
                LoadFromJsonToWord(level.ToArray<JSON>("words")[i]);
            }
            /*foreach(var answerJSON in level.ToArray<JSON>("words"))
            {
                allAnswersJSON.Add(answerJSON);            
                LoadFromJsonToWord(answerJSON);
            }*/
            if (GameController.levelNumber > PlayerPrefs.GetInt("LastPlayed"))
            {
                guessedWordsController.FillListAtStart(allAnswers, GameController.levelNumber);
            }
            else if (GameController.levelNumber < PlayerPrefs.GetInt("LastPlayed"))
            {
                guessedWordsController.GetGuessedWordsAtStart(allAnswers, GameController.levelNumber);
            }
            else if (GameController.levelNumber == PlayerPrefs.GetInt("LastPlayed"))
            {
                guessedWordsController.GetGuessedWordsLastLevel(allAnswers, GameController.levelNumber);
                isLoadLastLevel = true;
            }
            CreateField();
            CreateLetters();
            bezierConnection.CreateProgressLetter(letters.Length);
            gameController.GetAllWords();
            if (GameController.levelNumber > PlayerPrefs.GetInt("LastPlayed"))
            {
                PlayerPrefs.SetInt("LastPlayed", GameController.levelNumber);
            }
        }

    }

    void LoadFromJsonToWord(JSON answerJson)
    {
        AnswerContainer answer = new AnswerContainer();
        answer.id = answerJson.ToInt("id");
        answer.word = answerJson.ToString("word");
        answer.definintion = answerJson.ToString("def");
        answer.type = answerJson.ToInt("type");
        answer.additionalStars = answerJson.ToInt("additional stars");
        //answer.isGuessed = answerJson.ToBoolean("isGuessed");
        allAnswers.Add(answer);
    }

    List<string> GetAllWords()
    {
        List<string> allWords = new List<string>();
        for (int i = 0; i < allAnswers.Count; i++)
        {
            if (allAnswers[i].type == 2)
            {
                allWords.Add(allAnswers[i].word);
            }            
        }
        return allWords;
    }

    public List<string> GetAllExtraWords()
    {
        List<string> allWords = new List<string>();
        for (int i = 0; i < allAnswers.Count; i++)
        {
            if (allAnswers[i].type == 1)
            {
                allWords.Add(allAnswers[i].word);
                Debug.Log(allAnswers[i].word);
            }
        }
        return allWords;
    }

    List<string> SortAllWords(List<string> allWords)
    {
        List<string> sortWords = new List<string>();
        List<string> oneLengthWords = new List<string>();
        allWords.Sort((x, y) => x.Length.CompareTo(y.Length));
        oneLengthWords.Add(allWords[0]);
        for (int i = 1; i < allWords.Count; i++)
        {
            if (allWords[i].Length == allWords[i - 1].Length)
            {
                oneLengthWords.Add(allWords[i]);
            }
            else
            {
                oneLengthWords.Sort((x, y) => string.Compare(x, y));
                for (int j = 0; j < oneLengthWords.Count; j++)
                {
                    sortWords.Add(oneLengthWords[j]);
                }
                oneLengthWords.Clear();
                oneLengthWords.Add(allWords[i]);
            }

            if (i == allWords.Count - 1)
            {
                oneLengthWords.Sort((x, y) => string.Compare(x, y));
                for (int j = 0; j < oneLengthWords.Count; j++)
                {
                    sortWords.Add(oneLengthWords[j]);
                }
                oneLengthWords.Clear();
            }
        }
        allWords.Clear();
        for (int i = 0; i < sortWords.Count; i++)
        {
            allWords.Add(sortWords[i]);
        }
    
        return allWords;
    }

    void CreateField()
    {
        answerFieldController.CallGenerateField(SortAllWords(GetAllWords()));
        if (isLoadLastLevel)
        {
            isLoadLastLevel = false;
            OpenGuessedWords();
            
        }
        gameController.LoadHintLetters(allAnswers);
        if (GameController.levelNumber >= PlayerPrefs.GetInt("LastPlayed"))
        {
            AddAdditionalCoinsWords();
        }
    }

    void AddAdditionalCoinsWords()
    {
        for (int i = 0; i < allAnswers.Count; i++)
        {
            if (allAnswers[i].type == 2 && !allAnswers[i].isGuessed)
            {
                for (int j = 0; j < answerFieldController.AllSortWords.Count; j++)
                {
                    if (allAnswers[i].word == answerFieldController.AllSortWords[j])
                    {
                        if (allAnswers[i].additionalStars > 0)
                        {
                            answerFieldController.AllWordsObjects[j].GetComponent<WordController>().AdditionalCoins = allAnswers[i].additionalStars;
                            LetterOnFieldController[] allLetters = answerFieldController.AllWordsObjects[j].GetComponentsInChildren<LetterOnFieldController>();
                            int activeStars = allAnswers[i].additionalStars - answerFieldController.AllWordsObjects[j].GetComponent<WordController>().GuessedLettersNumber;
                            for (int k = allAnswers[i].additionalStars - 1; k > allAnswers[i].additionalStars - activeStars - 1; k--)
                            {
                                if (k < allLetters.Length)
                                {
                                    allLetters[k].EnableAdditionalCoinsSprite();
                                }                                                            
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    void OpenGuessedWords()
    {
        for (int i = 0; i < allAnswers.Count; i++)
        {
            if (allAnswers[i].type == 2 && allAnswers[i].isGuessed)
            {
                for (int j = 0; j < answerFieldController.AllSortWords.Count; j++)
                {
                    if (allAnswers[i].word == answerFieldController.AllSortWords[j])
                    {
                        answerFieldController.AllWordsObjects[j].GetComponent<WordController>().IsGuessed = true;
                        LetterOnFieldController[] allLetters = answerFieldController.AllWordsObjects[j].GetComponentsInChildren<LetterOnFieldController>();
                        for (int k = 0; k < allLetters.Length; k++)
                        {
                            allLetters[k].ChangeStateSprites();
                        }
                        break;
                    }
                }
            }
        }
    }

    void CreateLetters()
    {
        bezierLetters = Instantiate(allLettersOnFieldPrefabs[letters.Length - 3]) as GameObject;
        if (Mathf.Abs(uiController.defaultRatio - uiController.AspectRatio) > uiController.epsilon)
        {
            bezierLetters.transform.localScale = new Vector3(bezierLetters.transform.localScale.x * uiController.FinalRatio, bezierLetters.transform.localScale.y * uiController.FinalRatio, bezierLetters.transform.localScale.z);

        }
        allLetters = GameObject.FindGameObjectsWithTag("Letter");
        for (int i = 0; i < allLetters.Length; i++)
        {
            allLetters[i].transform.FindChild("LetterText").gameObject.GetComponent<TextMesh>().text = letters[i].ToString().ToUpper();
            allLetters[i].GetComponent<LetterBezierController>().Letter = letters[i];
            if (Mathf.Abs(uiController.defaultRatio - uiController.AspectRatio) > uiController.epsilon)
            { 
                allLetters[i].transform.position = new Vector3(allLetters[i].transform.position.x, allLetters[i].transform.position.y / uiController.FinalRatio, allLetters[i].transform.position.z);
            }
        }
    }

    public void EndLevel()
    {
        guessedWordsController.RefillListEndLevel(allAnswers, GameController.levelNumber);
        allAnswersJSON.Clear();
        allAnswers.Clear();
        Destroy(bezierLetters.gameObject);
        Invoke("WaitLoadLevel", 0.5f);
    }

    void WaitLoadLevel()
    {
        LoadLevelFromFile(GameController.levelNumber);
    }

    public void MakeWordGuessed(string word)
    {
        for (int i = 0; i < allAnswers.Count; i++)
        {
            if (allAnswers[i].word == word)
            {
                AnswerContainer answer = new AnswerContainer();
                allAnswers[i].isGuessed = true;
                allAnswersJSON[i].fields.Remove("isGuessed");
                allAnswersJSON[i].fields.Add("isGuessed", true);
            }
        }
        level.fields.Remove("words");
        level.fields.Add("words", allAnswersJSON);
    }

    void OnApplicationPause()
    {
        if (GameController.levelNumber > PlayerPrefs.GetInt("LevelsFinished"))
        {
            guessedWordsController.SaveGuessedWordsUnfinishedLevelOnPause(allAnswers, GameController.levelNumber);
        }
    }

    void OnApplicationQuit()
    {
        if (GameController.levelNumber > PlayerPrefs.GetInt("LevelsFinished"))
        {
            guessedWordsController.SaveGuessedWordsUnfinishedLevel(allAnswers, GameController.levelNumber);
        }
    }
}
