﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class HelpController : MonoBehaviour
{
    public int picturesCount;
    public float positionChangeValue;
    public float animationTime;
    public Image rightButtonImage;
    public Image leftButtonImage;
    public Sprite disabledRightButtonSprite;
    public Sprite disabledLeftButtonSprite;
    public Sprite enabledRightButtonSprite;
    public Sprite enabledLeftButtonSprite;
    public GameObject helpObject;
    public GameObject callObject;

    RectTransform helpRectTransform;
    //UIController uiController;
    int currentPicture = 0;
    bool isInMove;

    void Awake()
    {
        //uiController = FindObjectOfType<UIController>();
    }

    void Start()
    {
        helpRectTransform = GetComponent<RectTransform>();
        SetButtonSprites();
    }

    public void MoveToRight()
    {
        if (currentPicture < picturesCount - 1 && !isInMove)
        {
            float currentPosition = helpRectTransform.localPosition.x;
            currentPicture++;
            helpRectTransform.DOLocalMoveX(currentPosition - positionChangeValue, animationTime);
            SetButtonSprites();
            isInMove = true;
            Invoke("EndMove", animationTime);
        }
    }

    public void MoveToLeft()
    {
        if (currentPicture > 0 && !isInMove)
        {
            float currentPosition = helpRectTransform.localPosition.x;
            currentPicture--;
            helpRectTransform.DOLocalMoveX(currentPosition + positionChangeValue, animationTime);
            SetButtonSprites();
            isInMove = true;
            Invoke("EndMove", animationTime);
        }
    }

    void EndMove()
    {
        isInMove = false;
    }

    public void SetButtonSprites()
    {
        if (currentPicture <= 0)
        {
            leftButtonImage.sprite = disabledLeftButtonSprite;
        }
        else
        {
            leftButtonImage.sprite = enabledLeftButtonSprite;
        }

        if (currentPicture >= picturesCount - 1)
        {
            rightButtonImage.sprite = disabledRightButtonSprite;
        }
        else
        {
            rightButtonImage.sprite = enabledRightButtonSprite;
        }
    }

    public void ShowHelp()
    {
        if (!helpRectTransform)
        {
            helpRectTransform = GetComponent<RectTransform>();
        }
        currentPicture = 0;
        helpRectTransform.localPosition = new Vector3(0f, helpRectTransform.localPosition.y, helpRectTransform.localPosition.z);
        helpObject.SetActive(true);
        callObject.SetActive(false);
        //uiController.IsWindowOpen = true;
    }

    public void HideHelp()
    {        
        currentPicture = 0;
        helpRectTransform.localPosition = new Vector3(0f, helpRectTransform.localPosition.y, helpRectTransform.localPosition.z);
        SetButtonSprites();
        helpObject.SetActive(false);
        callObject.SetActive(true);
        //uiController.IsWindowOpen = false;
    }
}
