﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class GameController : MonoBehaviour
{
    public static int levelNumber = 1;
    public static int packNumber;
    public static int lastLevelInPack;
    public static int numberInPack;
    public static string packName;
    public static int popUpNumber;
    public int levelsCount;
    public int hintPrice;
    public float discountTime;
    public int[] extraWordsProgress;
    public int additionalCoinsMultiPlier;

    int points;
    int extraWordsLevel;
    int extraWordsCurrentLevelCount;
    static float discountTimeLeft;
    bool isReadyToClaim;
    bool isDiscount;
    AnswerFieldController answerFieldController;
    AnimationController animationController;
    UIController uiController;
    LevelCreationController levelCreationController;
    BezierConnection bezierConnection;
    GuessedWordsController guessedWordsController;
    CoinsController coinsController;
    public AudioSource sourceSound;
    public AudioClip levelCompleteClip;
    public AudioClip correctWordClip;
    public AudioClip wrongWordClip;
    WordController[] allWords;
    List<string> extraWords;

    private static GameController instance = null;

    public static GameController Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        instance = this;
        AdsController.instance.HideBanner();
        extraWordsCurrentLevelCount = PlayerPrefs.GetInt("ExtraWordsCurrentLevelCount");
        extraWordsLevel = PlayerPrefs.GetInt("ExtraWordsLevel");
        points = PlayerPrefs.GetInt("Points");
    }

    void Start()
    {
        answerFieldController = FindObjectOfType<AnswerFieldController>();
        animationController = FindObjectOfType<AnimationController>();
        uiController = FindObjectOfType<UIController>();
        levelCreationController = FindObjectOfType<LevelCreationController>();
        bezierConnection = FindObjectOfType<BezierConnection>();
        guessedWordsController = FindObjectOfType<GuessedWordsController>();
        coinsController = FindObjectOfType<CoinsController>();

        uiController.SetExtraWordsValue(extraWordsCurrentLevelCount, extraWordsProgress[extraWordsLevel]);
        uiController.SetHeaderName(packName, numberInPack);
        if (PlayerPrefs.GetInt("Discount") == 1)
        {
            StartDiscount();
        }
        else
        {
            StopDiscount();
        }
        if (extraWordsCurrentLevelCount >= extraWordsProgress[extraWordsLevel])
        {
            uiController.SetExtraWordsValue(extraWordsProgress[extraWordsLevel], extraWordsProgress[extraWordsLevel]);
            uiController.MakeReadyToClaim();
            isReadyToClaim = true;
        }
    }

    void Update()
    {
        if (isDiscount)
        {
            discountTimeLeft -= Time.deltaTime;
            uiController.UpdateDiscount(discountTimeLeft);
            if (discountTimeLeft < 0)
            {
                StopDiscount();
            }
        }
    }

    string GetUserConnectWord(List<LetterBezierController> connectedLetters)
    {
        string resultWord = "";
        for (int i = 0; i < connectedLetters.Count; i++)
        {
            resultWord += connectedLetters[i].Letter;
        }
        return resultWord;
    }

    public void CheckChosenWord(List<LetterBezierController> connectedLetters)
    {
        string chosenWord = GetUserConnectWord(connectedLetters);
        for (int i = 0; i < answerFieldController.AllSortWords.Count; i++)
        {
            if (answerFieldController.AllSortWords[i] == chosenWord)
            {
                WordController word = answerFieldController.AllWordsObjects[i].GetComponent<WordController>();
                if (!word.IsGuessed)
                {
                    sourceSound.PlayOneShot(correctWordClip);
                    animationController.OpenGuessedLetters(answerFieldController.AllWordsObjects[i]);
                    word.IsGuessed = true;
                    LetterOnFieldController[] allLettersInWord = word.GetComponentsInChildren<LetterOnFieldController>();
                    for (int j = 0; j < allLettersInWord.Length; j++)
                    {
                        allLettersInWord[j].IsGuessed = true;
                        word.GuessedLettersNumber = allLettersInWord.Length;
                    }
                    levelCreationController.MakeWordGuessed(word.Word);
                    bezierConnection.ChangeWordProgressObjectColor(new Color(0f, 0.6f, 0.1f, 0.6f));
                    if (word.AdditionalCoins > 0)
                    {
                        AddAdditionalCoins(word.AdditionalCoins, allLettersInWord);
                    }
                    Invoke("CheckEndOfLevel", 1f);
                }
                else
                {
                    bezierConnection.ChangeWordProgressObjectColor(new Color(0.8f, 0.75f, 0f, 0.7f));
                }
                return;
            }
        }

        for (int i = 0; i < extraWords.Count; i++)
        {
            if (extraWords[i] == chosenWord)
            {
                for (int j = 0; j < levelCreationController.AllAnswers.Count; j++)
                {
                    if (extraWords[i] == levelCreationController.AllAnswers[j].word && !levelCreationController.AllAnswers[j].isGuessed && !isReadyToClaim)
                    {
                        animationController.ExtraWordsAnimation();
                        levelCreationController.AllAnswers[j].isGuessed = true;
                        levelCreationController.MakeWordGuessed(chosenWord);
                        bezierConnection.ChangeWordProgressObjectColor(new Color(0f, 0.1f, 0.7f, 0.7f));
                        AddExtraWord();
                        return;
                    }
                    else if (extraWords[i] == levelCreationController.AllAnswers[j].word && !levelCreationController.AllAnswers[j].isGuessed && isReadyToClaim)
                    {
                        bezierConnection.ChangeWordProgressObjectColor(new Color(0.8f, 0.75f, 0f, 0.7f));
                        return;
                    }
                    else if (extraWords[i] == levelCreationController.AllAnswers[j].word && levelCreationController.AllAnswers[j].isGuessed)
                    {
                        bezierConnection.ChangeWordProgressObjectColor(new Color(0.8f, 0.75f, 0f, 0.7f));
                        return;
                    }
                }
            }
        }

        sourceSound.PlayOneShot(wrongWordClip);
        bezierConnection.ChangeWordProgressObjectColor(new Color(0.7f, 0f, 0f, 0.8f));
    }

    void AddAdditionalCoins(int coinsCount, LetterOnFieldController[] allLettersInWord)
    {
        coinsController.ChangeCoinsValue(additionalCoinsMultiPlier * coinsCount);
        for (int j = 0; j < allLettersInWord.Length; j++)
        {
            allLettersInWord[j].DisableAdditionalCoinsSprite();
        }
    }

    void AddExtraWord()
    {
        extraWordsCurrentLevelCount++;
        if (extraWordsCurrentLevelCount >= extraWordsProgress[extraWordsLevel])
        {
            uiController.SetExtraWordsValue(extraWordsProgress[extraWordsLevel], extraWordsProgress[extraWordsLevel]);
            uiController.MakeReadyToClaim();
            isReadyToClaim = true;
        }
        else
        {
            if (!isReadyToClaim)
            {
                uiController.SetExtraWordsValue(extraWordsCurrentLevelCount, extraWordsProgress[extraWordsLevel]);
            }
        }
        PlayerPrefs.SetInt("ExtraWordsCurrentLevelCount", extraWordsCurrentLevelCount);
    }

    public void Claim()
    {
        extraWordsCurrentLevelCount = 0;
        if (extraWordsLevel < extraWordsProgress.Length - 1)
        {
            extraWordsLevel++;
            PlayerPrefs.SetInt("ExtraWordsLevel", extraWordsLevel);
        }
        isReadyToClaim = false;
        coinsController.ChangeCoinsValue(extraWordsProgress[extraWordsLevel - 1]);
        uiController.ChangeCoinsText(PlayerPrefs.GetInt("CoinsCount"));
        uiController.Claim();
        uiController.SetExtraWordsValue(extraWordsCurrentLevelCount, extraWordsProgress[extraWordsLevel]);
        PlayerPrefs.SetInt("ExtraWordsCurrentLevelCount", extraWordsCurrentLevelCount);
    }

    public void GetAllWords()
    {
        allWords = FindObjectsOfType<WordController>();
        extraWords = levelCreationController.GetAllExtraWords();
    }

    void CheckEndOfLevel()
    {
        for (int i = 0; i < allWords.Length; i++)
        {
            if (!allWords[i].IsGuessed)
            {
                return;
            }
        }
        sourceSound.PlayOneShot(levelCompleteClip);
        if (levelNumber > PlayerPrefs.GetInt("LevelsFinished"))
        {
            int oldPoints = points;
            points += 4 + packNumber;
            PlayerPrefs.SetInt("Points", points);
            PlayerPrefs.SetInt("LevelsFinished", levelNumber);
            if (levelNumber == levelsCount)
            {
                uiController.ShowEndPanelWithPoints(oldPoints, points, levelNumber, lastLevelInPack, true);
            }
            else
            {
                uiController.ShowEndPanelWithPoints(oldPoints, points, levelNumber, lastLevelInPack, false);
            }                
        }
        else
        {
            if (levelNumber == levelsCount)
            {
                uiController.ShowEndLevelPanel(points, levelNumber, lastLevelInPack, true);
            }
            else
            {
                uiController.ShowEndLevelPanel(points, levelNumber, lastLevelInPack, false);
            }
        }

        /*if (levelNumber > 1)
        {
            RatePopup.ShowPopup();
            AdsController.instance.showOnFocus = false;
            Invoke("WaitFocus", 30f);
        }*/
    }



    public void StartNextLevel()
    {
        answerFieldController.EndLevel();
        uiController.HideEndLevelPanel();
        levelCreationController.EndLevel();
        if (levelNumber == levelsCount)
        {
            SceneManager.LoadScene(0);
        }
        else
        {
            levelNumber++;
        }
        
        if (levelNumber > lastLevelInPack)
        {
            AdsController.instance.RevealBanner();
            ScrollMenuController.isEndOfPackLoad = true;
            SceneManager.LoadScene(1);
            packNumber++;
        }
        if (levelNumber == 2 && PlayerPrefs.GetInt("IsRate") == 0)
        {
            AdsController.instance.RevealBanner();
            ScrollMenuController.isEndOfPackLoad = true;
            SceneManager.LoadScene(1);
        }

        numberInPack++;
        uiController.SetHeaderName(packName, numberInPack);
        if (AdsController.instance != null)
        {
            if (AdsController.instance.IsLaunchScreenAdOn == 1)
            {
                AdsController.instance.ShowInterstitial(interstitialType.interstitialLaunch);
            }
            else if (AdsController.instance.IsLaunchScreenAdOn == 2)
            {
                AdsController.instance.ShowAppLovinInterstitial();
            }
            else if (AdsController.instance.IsLaunchScreenAdOn == 3)
            {
                //FACEBOOK AD SHOW
            }
        }
    }

    public void GoToMenuShittyUserDontWantRate()
    {
        int oldPoints = points - (4 + packNumber);
        PlayerPrefs.SetInt("LevelsFinished", levelNumber - 1);
        PlayerPrefs.SetInt("Points", oldPoints);
        Quit();
    }

    public void Hint()
    {
        if (coinsController.CoinsCount < hintPrice)
        {
            uiController.OpenCoinWindow();
            return;
        }
        int minGuessedLetters = int.MaxValue;
        for (int i = 0; i < answerFieldController.AllWordsControllers.Count; i++)
        {
            if (!answerFieldController.AllWordsControllers[i].IsGuessed && !answerFieldController.AllWordsControllers[i].AllLettersOpened)
            {
                if (minGuessedLetters > answerFieldController.AllWordsControllers[i].GuessedLettersNumber)
                {
                    minGuessedLetters = answerFieldController.AllWordsControllers[i].GuessedLettersNumber;
                }
            }
        }

        for (int i = 0; i < answerFieldController.AllWordsControllers.Count; i++)
        {
            if (!answerFieldController.AllWordsControllers[i].IsGuessed && !answerFieldController.AllWordsControllers[i].AllLettersOpened)
            {
                if (minGuessedLetters == answerFieldController.AllWordsControllers[i].GuessedLettersNumber && coinsController.CoinsCount >= hintPrice)
                {
                    LetterOnFieldController[] allLetters = answerFieldController.AllWordsControllers[i].GetComponentsInChildren<LetterOnFieldController>();
                    allLetters[minGuessedLetters].HintLetter();
                    answerFieldController.AllWordsControllers[i].GuessedLettersNumber++;
                    coinsController.ChangeCoinsValue(-hintPrice);
                    uiController.ChangeCoinsText(PlayerPrefs.GetInt("CoinsCount"));
                    if (answerFieldController.AllWordsControllers[i].AdditionalCoins > 0)
                    {
                        allLetters[minGuessedLetters].DisableAdditionalCoinsSprite();
                        answerFieldController.AllWordsControllers[i].AdditionalCoins--;
                    }
                    if (answerFieldController.AllWordsControllers[i].GuessedLettersNumber == allLetters.Length)
                    {
                        answerFieldController.AllWordsControllers[i].AllLettersOpened = true;
                    }
                    guessedWordsController.SaveHintLetters(answerFieldController.AllWordsControllers[i].Word, levelCreationController.AllAnswers, levelNumber, answerFieldController.AllWordsControllers[i].GuessedLettersNumber);
                    return;
                }
            }
        }        
    }

    public void LoadHintLetters(List<AnswerContainer> allAnswers)
    {
        for (int i = 0; i < allAnswers.Count; i++)
        {
            for (int j = 0; j < guessedWordsController.AllGuessingWordStates.allGuessingWordStates.Count; j++)
            {
                if (allAnswers[i].id == guessedWordsController.AllGuessingWordStates.allGuessingWordStates[j].id)
                {
                    for (int k = 0; k < answerFieldController.AllWordsControllers.Count; k++)
                    {
                        if (answerFieldController.AllWordsControllers[k].Word == allAnswers[i].word)
                        {
                            answerFieldController.AllWordsControllers[k].GuessedLettersNumber = guessedWordsController.AllGuessingWordStates.allGuessingWordStates[j].hintLettersCount;
                            LetterOnFieldController[] allLetters = answerFieldController.AllWordsControllers[k].GetComponentsInChildren<LetterOnFieldController>();
                            LoadHintLetter(allLetters, k);
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }

    void LoadHintLetter(LetterOnFieldController[] allLetters, int wordNumber)
    {
        for (int i = 0; i < allLetters.Length; i++)
        {
            if (i < answerFieldController.AllWordsControllers[wordNumber].GuessedLettersNumber)
            {
                allLetters[i].HintLetter();
            }
            else
            {
                break;
            }
        }
        if (answerFieldController.AllWordsControllers[wordNumber].GuessedLettersNumber == allLetters.Length)
        {
            answerFieldController.AllWordsControllers[wordNumber].AllLettersOpened = true;
        }
    }

    void StartDiscount()
    {
        DateTime currentDate = DateTime.Now;
        long temp = Convert.ToInt64(PlayerPrefs.GetString("DiscountStartTime"));
        DateTime oldDate = DateTime.FromBinary(temp);        
        hintPrice = 12;
        isDiscount = true;
        PlayerPrefs.SetInt("Discount", 1);
        discountTimeLeft = discountTime - (int)currentDate.Subtract(oldDate).TotalSeconds;
        uiController.StartDiscount();
    }

    void StopDiscount()
    {
        hintPrice = 25;
        isDiscount = false;
        PlayerPrefs.SetInt("Discount", 0);
        uiController.StopDiscount();
    }

    public void Quit()
    {
        if (levelNumber > PlayerPrefs.GetInt("LevelsFinished"))
        {
            guessedWordsController.SaveGuessedWordsUnfinishedLevel(levelCreationController.AllAnswers, levelNumber);
        }
        if (AdsController.instance != null)
        {
            if (AdsController.instance.IsLaunchScreenAdOn == 1)
            {
                AdsController.instance.ShowInterstitial(interstitialType.interstitialLaunch);
            }
            else if (AdsController.instance.IsLaunchScreenAdOn == 2)
            {
                AdsController.instance.ShowAppLovinInterstitial();
            }
            else if (AdsController.instance.IsLaunchScreenAdOn == 3)
            {
                //FACEBOOK AD SHOW
            }
        }
        AdsController.instance.RevealBanner();
        SceneManager.LoadScene(2);
    }
}
