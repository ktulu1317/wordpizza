﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LetterOnFieldController : MonoBehaviour
{
    public Sprite letterHideSprite;
    public Sprite letterOpenSprite;
    char letter;
    bool isGuessed;

    public char Letter
    {
        get { return letter; }
        set { letter = value; }
    }

    public bool IsGuessed
    {
        get { return isGuessed; }
        set { isGuessed = value; }
    }

    public void ChangeStateAnimation(float time)
    {
        float oldScale = this.transform.localScale.x;
        Invoke("ChangeStateSprites", time);
        Sequence seq = DOTween.Sequence();
        seq.Append(this.transform.DOScale(0f, time));
        seq.Append(this.transform.DOScale(oldScale, time));
    }

    public void HintLetter()
    {
        this.transform.FindChild("LetterText").gameObject.SetActive(true);
    }

    public void ChangeStateSprites()
    {
        this.GetComponent<SpriteRenderer>().sprite = letterOpenSprite;
        this.transform.FindChild("LetterText").gameObject.SetActive(true);
    }

    public void EnableAdditionalCoinsSprite()
    {
        this.transform.FindChild("AdditionalCoin").gameObject.SetActive(true);
    }

    public void DisableAdditionalCoinsSprite()
    {
        this.transform.FindChild("AdditionalCoin").gameObject.SetActive(false);
    }
}
