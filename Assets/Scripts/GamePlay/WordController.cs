﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordController : MonoBehaviour
{
    string word;
    int guessedLettersNumber;
    int additionalCoins;
    bool isGuessed;
    bool allLettersOpened;

    public string Word
    {
        get { return word; }
        set { word = value; }
    }

    public int GuessedLettersNumber
    {
        get { return guessedLettersNumber; }
        set { guessedLettersNumber = value; }
    }

    public int AdditionalCoins
    {
        get { return additionalCoins; }
        set { additionalCoins = value; }
    }

    public bool IsGuessed
    {
        get { return isGuessed; }
        set { isGuessed = value; }
    }

    public bool AllLettersOpened
    {
        get { return allLettersOpened; }
        set { allLettersOpened = value; }
    }
}
