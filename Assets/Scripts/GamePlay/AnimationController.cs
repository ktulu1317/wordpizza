﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AnimationController : MonoBehaviour
{
    public float openLetterAnimSpeed;
    public float openLetterAnimWait;
    public float shuffleTime;
    public float extraWordFinalPosition;
    public float extraWordTime;
    public float coinAnimationTime;
    public Vector3 centerShufflePosition;
    public GameObject extraWord;

    LevelCreationController levelCreationController;
    UIController uiController;
    bool isInShuffle;

    void Start()
    {
        levelCreationController = FindObjectOfType<LevelCreationController>();
        uiController = FindObjectOfType<UIController>();
        extraWord.SetActive(false);
        if (Mathf.Abs(uiController.defaultRatio - uiController.AspectRatio) > uiController.epsilon)
        {
            extraWord.transform.position = new Vector3(extraWord.transform.position.x * (uiController.defaultRatio / uiController.AspectRatio), extraWord.transform.position.y / uiController.FinalRatio, extraWord.transform.position.z);
        }
    }

    public void OpenGuessedLetters(GameObject word)
    {
        LetterOnFieldController[] allLetters = word.GetComponentsInChildren<LetterOnFieldController>();
        StartCoroutine(StartOpenLetterAnim(allLetters));
    }

    IEnumerator StartOpenLetterAnim(LetterOnFieldController[] allLetters)
    {
        for (int i = 0; i < allLetters.Length; i++)
        {
            allLetters[i].ChangeStateAnimation(openLetterAnimSpeed);
            yield return new WaitForSeconds(openLetterAnimWait);
        }
    }

    public void Shuffle()
    {
        if (!isInShuffle)
        {
            isInShuffle = true;
            int[] indexes = ShuffleIndexes();
            ShuffleLetters(indexes);
        }
    }

    int[] ShuffleIndexes()
    {
        int[] indexes = new int[levelCreationController.AllLetters.Length];
        for (int i = 0; i < indexes.Length; i++)
        {
            indexes[i] = i;
        }

        for (int i = 0; i < indexes.Length; i++)
        {
            int randomIndex = Random.Range(0, indexes.Length);
            int temp = indexes[randomIndex];
            indexes[randomIndex] = indexes[i];
            indexes[i] = temp;
        }

        return indexes;
    }

    void ShuffleLetters(int[] indexes)
    {
        Invoke("SetIsShuffleFalse", shuffleTime * 2f);
        Sequence seq = DOTween.Sequence();
        seq.SetId<Sequence>("shuffle");
        seq.Append(levelCreationController.AllLetters[0].transform.DOLocalMove(centerShufflePosition, shuffleTime));
        for (int i = 1; i < indexes.Length; i++)
        {
            seq.Join(levelCreationController.AllLetters[i].transform.DOLocalMove(centerShufflePosition, shuffleTime));
        }
        seq.Append(levelCreationController.AllLetters[0].transform.DOLocalMove(levelCreationController.AllLetters[indexes[0]].transform.localPosition, shuffleTime));
        for (int i = 1; i < indexes.Length; i++)
        {
            seq.Join(levelCreationController.AllLetters[i].transform.DOLocalMove(levelCreationController.AllLetters[indexes[i]].transform.localPosition, shuffleTime));
        }
    }

    void SetIsShuffleFalse()
    {
        isInShuffle = false;
    }

    public void ExtraWordsAnimation()
    {
        extraWord.SetActive(true);
        float startPosition = extraWord.transform.position.y;
        extraWord.transform.DOMoveY(extraWordFinalPosition, extraWordTime);
        StartCoroutine(EndExtraWordAnimation(startPosition, extraWordTime));
    }

    IEnumerator EndExtraWordAnimation(float startPosition, float time)
    {
        yield return new WaitForSeconds(time);
        extraWord.SetActive(false);
        extraWord.transform.position = new Vector3(extraWord.transform.position.x, startPosition, extraWord.transform.position.z);
    }

    public void AnimateCoin(RectTransform coin, Vector3 coinStartPosition, Vector3 endPosition)
    {
        coin.DOMove(endPosition, coinAnimationTime);
        StartCoroutine(BackCoin(coin, coinStartPosition));
    }

    IEnumerator BackCoin(RectTransform coin, Vector3 coinStartPosition)
    {
        yield return new WaitForSeconds(coinAnimationTime);
        coin.DOMove(coinStartPosition, 0f);
    }
}
