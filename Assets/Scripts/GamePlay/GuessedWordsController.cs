﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class GuessingWordState
{
    public int id;
    public bool isGuessed;
    public int hintLettersCount;
}

[System.Serializable]
public class ListGuessingWordState
{
    public List<GuessingWordState> allGuessingWordStates = new List<GuessingWordState>();
}

public class GuessedWordsController : MonoBehaviour
{
    ListGuessingWordState allGuessingWordStates = new ListGuessingWordState();

    public ListGuessingWordState AllGuessingWordStates
    {
        get { return allGuessingWordStates; }
        set { allGuessingWordStates = value; }
    }

    public void FillListAtStart(List<AnswerContainer> allAnswers, int levelNumber)
    {
        for (int i = 0; i < allAnswers.Count; i++)
        {
            GuessingWordState wordState = new GuessingWordState();
            wordState.id = allAnswers[i].id;
            wordState.isGuessed = false;
            allGuessingWordStates.allGuessingWordStates.Add(wordState);
            allAnswers[i].isGuessed = false;
        }
        WriteToFile(levelNumber);
    }

    public void GetGuessedWordsAtStart(List<AnswerContainer> allAnswers, int levelNumber)
    {
        ReadFromFileAtStart(levelNumber);
        for (int i = 0; i < allAnswers.Count; i++)
        {
            for (int j = 0; j < allGuessingWordStates.allGuessingWordStates.Count; j++)
            {
                if (allGuessingWordStates.allGuessingWordStates[j].id == allAnswers[i].id)
                {
                    if (allAnswers[i].type == 1)
                    {
                        allAnswers[i].isGuessed = allGuessingWordStates.allGuessingWordStates[j].isGuessed;
                    }
                    else
                    {
                        allAnswers[i].isGuessed = false;
                    }
                }
            }
        }
    }

    public void GetGuessedWordsLastLevel(List<AnswerContainer> allAnswers, int levelNumber)
    {
        ReadFromFileAtStart(levelNumber);
        for (int i = 0; i < allAnswers.Count; i++)
        {
            for (int j = 0; j < allGuessingWordStates.allGuessingWordStates.Count; j++)
            {
                if (allGuessingWordStates.allGuessingWordStates[j].id == allAnswers[i].id)
                {
                     allAnswers[i].isGuessed = allGuessingWordStates.allGuessingWordStates[j].isGuessed;
                }
            }
        }
    }

    public void SaveGuessedWordsUnfinishedLevel(List<AnswerContainer> allAnswers, int levelNumber)
    {
        for (int i = 0; i < allAnswers.Count; i++)
        {
            for (int j = 0; j < allGuessingWordStates.allGuessingWordStates.Count; j++)
            {
                if (allGuessingWordStates.allGuessingWordStates[j].id == allAnswers[i].id)
                {
                     allGuessingWordStates.allGuessingWordStates[j].isGuessed = allAnswers[i].isGuessed;
                }
            }
        }
        WriteToFile(levelNumber);
        allGuessingWordStates.allGuessingWordStates.Clear();
    }

    public void SaveGuessedWordsUnfinishedLevelOnPause(List<AnswerContainer> allAnswers, int levelNumber)
    {
        for (int i = 0; i < allAnswers.Count; i++)
        {
            for (int j = 0; j < allGuessingWordStates.allGuessingWordStates.Count; j++)
            {
                if (allGuessingWordStates.allGuessingWordStates[j].id == allAnswers[i].id)
                {
                    allGuessingWordStates.allGuessingWordStates[j].isGuessed = allAnswers[i].isGuessed;
                }
            }
        }
        WriteToFile(levelNumber);
    }

    void WriteToFile(int levelNumber)
    {
        string s = JsonUtility.ToJson(allGuessingWordStates);
        File.WriteAllText(Application.persistentDataPath + Path.DirectorySeparatorChar + "LevelGuessed" + levelNumber +".json", s);
    }

    void ReadFromFileAtStart(int levelNumber)
    {
        string s = File.ReadAllText(Application.persistentDataPath + Path.DirectorySeparatorChar + "LevelGuessed" + levelNumber + ".json");
        allGuessingWordStates = JsonUtility.FromJson<ListGuessingWordState>(s);
    }

    public void RefillListEndLevel(List<AnswerContainer> allAnswers, int levelNumber)
    {
        for (int i = 0; i < allAnswers.Count; i++)
        {
            for (int j = 0; j < allGuessingWordStates.allGuessingWordStates.Count; j++)
            {
                if (allGuessingWordStates.allGuessingWordStates[j].id == allAnswers[i].id)
                {
                    if (allAnswers[i].type == 1)
                    {
                        allGuessingWordStates.allGuessingWordStates[j].isGuessed = allAnswers[i].isGuessed;
                    }
                }                    
            }                
        }
        WriteToFile(levelNumber);
        allGuessingWordStates.allGuessingWordStates.Clear();
    }

    public void SaveHintLetters(string word, List<AnswerContainer> allAnswers, int levelNumber, int lettersNumber)
    {
        for (int i = 0; i < allAnswers.Count; i++)
        {
            if (word == allAnswers[i].word)
            {
                allGuessingWordStates.allGuessingWordStates[i].hintLettersCount = lettersNumber;
            }
        }
    }
}
