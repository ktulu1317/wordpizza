﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersPrefsController : MonoBehaviour
{
    void Start()
    {
        /*PlayerPrefs.DeleteKey("LastPlayed");
          PlayerPrefs.DeleteKey("LevelsFinished");
          PlayerPrefs.DeleteKey("ExtraWordsLevel");
          PlayerPrefs.DeleteKey("ExtraWordsCurrentLevelCount");
          PlayerPrefs.DeleteKey("Points");*/

        if (!PlayerPrefs.HasKey("LastPlayed"))
        {
            PlayerPrefs.SetInt("LastPlayed", 0);
        }
        if (!PlayerPrefs.HasKey("LevelsFinished"))
        {
            PlayerPrefs.SetInt("LevelsFinished", 0);
        }
        if (!PlayerPrefs.HasKey("ExtraWordsLevel"))
        {
            PlayerPrefs.SetInt("ExtraWordsLevel", 0);
        }
        if (!PlayerPrefs.HasKey("ExtraWordsCurrentLevelCount"))
        {
            PlayerPrefs.SetInt("ExtraWordsCurrentLevelCount", 0);
        }
        if (!PlayerPrefs.HasKey("Points"))
        {
            PlayerPrefs.SetInt("Points", 0);
        }
        if (!PlayerPrefs.HasKey("Discount"))
        {
            PlayerPrefs.SetInt("Discount", 0);
        }
        if (!PlayerPrefs.HasKey("BackgroundMusic"))
        {
            PlayerPrefs.SetInt("BackgroundMusic", 1);
        }
        if (!PlayerPrefs.HasKey("Sound"))
        {
            PlayerPrefs.SetInt("Sound", 1);
        }
        if (!PlayerPrefs.HasKey("IsRate"))
        {
            PlayerPrefs.SetInt("IsRate", 0);
        }
    }
}
