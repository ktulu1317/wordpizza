﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnswerFieldController : MonoBehaviour
{
    public float fieldWidth;
    public float fieldHeight;
    public float maxLetterSize;
    public float minLetterSize;
    public float maxOneRowWordsCount;
    public float maxTwoRowWordsCount;
    public float maxThreeRowWordsCount;
    public float gapBetweenLettersPercent;
    public float minGap;
    public float minGapBetweenWords;
    public float maxGap;
    public float maxHorizontalGap;
    public float letterPrefabSize;
    public GameObject wordPrefab;
    public GameObject lettersPrefab;
    public GameObject columnPrefab;

    List<string> allSortWords;
    List<GameObject> allWordsObjects = new List<GameObject>();
    List<WordController> allWordsControllers = new List<WordController>();

    public List<string> AllSortWords
    {
        get { return allSortWords; }
        set { allSortWords = value; }
    }

    public List<GameObject> AllWordsObjects
    {
        get { return allWordsObjects; }
        set { allWordsObjects = value; }
    }

    public List<WordController> AllWordsControllers
    {
        get { return allWordsControllers; }
        set { allWordsControllers = value; }
    }

    void Start ()
    {

	}

    public void CallGenerateField(List<string> allWords)
    {
        allSortWords = allWords;
        GenerateField(allSortWords, maxLetterSize, minLetterSize, fieldWidth, fieldHeight);
    }
	
    void GenerateField(List<string> allSortUsingWords, float maxLetterSize, float minLetterSize, float fieldWidth, float fieldHeight)
    {
        int columnCount = GetColumnNumbers(allSortUsingWords.Count);
        int[] wordsInColumns = GetAllWordColumnsCount(columnCount, allSortUsingWords.Count);
        int longestLineSize = GetLongestLine(allSortUsingWords, wordsInColumns);
        float maxVerticalSize = (fieldHeight - (minGap * 2 + (minGapBetweenWords * (wordsInColumns[0] - 1)))) / wordsInColumns[0];
        float maxHorizontalSize = (fieldWidth - (minGap * (columnCount + 1))) / (longestLineSize + (gapBetweenLettersPercent * (longestLineSize - 1)));
        float lettersSize;
        if (maxHorizontalSize < maxVerticalSize)
        {
            lettersSize = maxHorizontalSize;
        }
        else
        {
            lettersSize = maxVerticalSize;
        }
        float gapValue = lettersSize * gapBetweenLettersPercent;
        if (columnCount == 1)
        {
            SetOneColumnWordsPositions(gapValue, lettersSize, allSortUsingWords, wordsInColumns[0]);
        }
        else
        {
            SetSeveralColumnsWordsPositions(gapValue, lettersSize, allSortUsingWords, wordsInColumns, columnCount);
        }
    }

    int GetColumnNumbers(int wordsCount)
    {
        if (wordsCount > 0 && wordsCount <= maxOneRowWordsCount)
        {
            return 1;
        }
        else if (wordsCount > maxOneRowWordsCount && wordsCount <= maxTwoRowWordsCount)
        {
            return 2;
        }
        else if (wordsCount > maxTwoRowWordsCount && wordsCount <= maxThreeRowWordsCount)
        {
            return 3;
        }
        else
        {
            return -1;
        }
    }

    int[] GetAllWordColumnsCount(int columnCount, int wordsCount)
    {
        float columnCountFloat = (float)columnCount;
        float wordsRest = (float)wordsCount;
        int[] wordsInColumns = new int[columnCount];
        for (int i = 0; i < columnCount; i++)
        {
            int wordsInColumn = Mathf.CeilToInt(wordsRest / columnCountFloat);
            wordsInColumns[i] = wordsInColumn;
            wordsRest -= wordsInColumn;
            columnCountFloat--;
        }
        return wordsInColumns;
    }

    int GetLongestLine(List<string> allWords, int[] wordsInColumns)
    {
        int longestLineSize = 0;
        for (int i = 0; i < wordsInColumns.Length; i++)
        {
            int index = 0;
            for (int j = 0; j < i + 1; j++)
            {
                index += wordsInColumns[j];
            }
            longestLineSize += allWords[index - 1].Length;
        }
        return longestLineSize;
    }

    float GetGapBetweenLetters(float startLetterLength, int numberOfGaps)
    {
        float gapValue = startLetterLength * gapBetweenLettersPercent;
        float allGapsSize = gapValue * numberOfGaps;
        float oldAllGapsSize = allGapsSize;
        float gapsDifference = allGapsSize;
        float newLengthValue = startLetterLength;
        bool onReduce = true;
        do
        {
            if (onReduce)
            {
                newLengthValue = newLengthValue - gapsDifference;
            }
            else
            {
                newLengthValue = newLengthValue + gapsDifference;
            }
            gapValue = newLengthValue * gapBetweenLettersPercent;
            allGapsSize = gapValue * numberOfGaps;
            gapsDifference = Mathf.Abs(oldAllGapsSize - allGapsSize);
            oldAllGapsSize = allGapsSize;
            onReduce = !onReduce;
        } while (gapsDifference > 1);
        gapValue = Mathf.Round(gapValue * 100f) / 100f;
        return gapValue;
    }

    void SetOneColumnWordsPositions(float gapSize, float letterSize, List<string> allSortUsingWords, int wordsInColumn)
    {
        float verticalPosition = GetFirstWordVerticalPositionOneColumn(letterSize);
        float letterScale = letterSize / letterPrefabSize;
        //List<GameObject> allWords = new List<GameObject>();
        for (int i = 0; i < wordsInColumn; i++)
        {
            float letterPosition = GetFirstLetterPositionOneColumn(allSortUsingWords[i], letterSize, gapSize);
            GameObject newWord = CreateWord(verticalPosition, allSortUsingWords, i);
            allWordsObjects.Add(newWord);
            allWordsControllers.Add(newWord.GetComponent<WordController>());
            for (int j = 0; j < allSortUsingWords[i].Length; j++)
            {
                CreateLetter(newWord, ref letterPosition, letterSize, gapSize, letterScale, allSortUsingWords, i, j);
            }
            verticalPosition -= (letterSize + gapSize) / 100f;
        }
        RemoveDifferenceInGaps(allWordsObjects[0], allWordsObjects[allWordsObjects.Count - 1], letterSize, allWordsObjects);
        RemoveOverMaxGaps(allWordsObjects[0], allWordsObjects, letterSize);
    }

    float GetFirstLetterPositionOneColumn(string word, float letterSize, float gapSize)
    {
        float firstLetterPosition;
        float numberOfLetters = (word.Length - 1f) / 2f;
        firstLetterPosition = 0 - (letterSize * numberOfLetters) - (gapSize * numberOfLetters);
        return firstLetterPosition;
    }

    float GetFirstWordVerticalPositionOneColumn(float lettersSize)
    {
        float maxVerticalPosition = fieldHeight / 2f - lettersSize / 2f;
        float verticalPosition = maxVerticalPosition - minGap;
        return verticalPosition / 100f;
    }

    void RemoveDifferenceInGaps(GameObject firstWord, GameObject lastWord, float letterSize, List<GameObject> allWords)
    {
        float maxVerticalPosition = fieldHeight / 2f - letterSize / 2f;
        float minVerticalPosition = -maxVerticalPosition;
        float gapFirst = maxVerticalPosition / 100f - firstWord.transform.localPosition.y;
        float gapLast = minVerticalPosition / 100f - lastWord.transform.localPosition.y;
        float gapDifference = (gapFirst + gapLast) / 2f;
        for (int i = 0; i < allWords.Count; i++)
        {
            allWords[i].transform.localPosition = new Vector3(allWords[i].transform.localPosition.x, allWords[i].transform.localPosition.y + gapDifference, allWords[i].transform.localPosition.z);
        }
    }

    void RemoveOverMaxGaps(GameObject firstWord, List<GameObject> allWords, float letterSize)
    {
        float maxVerticalPosition = fieldHeight / 2f - letterSize / 2f;
        if (firstWord.transform.localPosition.y < (maxVerticalPosition - maxGap) / 100f)
        {
            float position = (maxVerticalPosition - maxGap) / 100f;
            float step = position * 2f / (allWords.Count - 1);
            for (int i = 0; i < allWords.Count; i++)
            {
                allWords[i].transform.localPosition = new Vector3(allWords[i].transform.localPosition.x, position - (step * i), allWords[i].transform.localPosition.z);
            }
        }
    }

    void SetSeveralColumnsWordsPositions(float gapSize, float letterSize, List<string> allSortUsingWords, int[] wordsInColumns, int columnsCount)
    {
        int[] longestWordsLettersCount = FindAllColumnsWidth(wordsInColumns, allSortUsingWords);
        float[] columnsWidth = FindLongestWordsInColumnLength(longestWordsLettersCount, letterSize, gapSize);
        float verticalPosition = GetFirstWordVerticalPositionOneColumn(letterSize);
        float letterScale = letterSize / letterPrefabSize;
        //List<GameObject> allWords = new List<GameObject>();
        List<GameObject> allColumns = new List<GameObject>();
        int startIndex = 0;
        for (int i = 0; i < wordsInColumns.Length; i++)
        {
            SetWordsInOneColumnForSeveralColumns(gapSize, letterSize, allSortUsingWords, wordsInColumns[i], startIndex, verticalPosition, allWordsObjects, allColumns, letterScale, allWordsControllers);
            startIndex += wordsInColumns[i];
        }
        SetColumnsPositionsHorizontal(allColumns, letterSize, columnsWidth);
        RemoveDifferenceInGaps(allWordsObjects[0], allWordsObjects[wordsInColumns[0] - 1], letterSize, allColumns);
        RemoveOverMaxVerticalGapsSeveralColumns(allWordsObjects[0], allWordsObjects, letterSize, allColumns[0], wordsInColumns);
        RemoveDifferenceInColumnsGaps(allColumns, letterSize, columnsWidth);
        RemoveOverMaxHorizontalGapsSeveralColumns(allColumns, letterSize, columnsWidth, columnsCount);
        int a = 0;
        if (allWordsObjects[1].transform.localPosition.y - allWordsObjects[0].transform.localPosition.y < -4)
        {
            float gap = (2f + (allWordsObjects[1].transform.localPosition.y - allWordsObjects[0].transform.localPosition.y)) / 2f;
            for (int i = 0; i < wordsInColumns[0]; i++)
            {
                if (i % 2 == 0)
                {
                    a++;
                    allWordsObjects[i].transform.localPosition = new Vector3(allWordsObjects[i].transform.localPosition.x, -allColumns[0].transform.localPosition.y + (1.5f * a), allWordsObjects[i].transform.localPosition.z);
                }
                else
                {
                    allWordsObjects[i].transform.localPosition = new Vector3(allWordsObjects[i].transform.localPosition.x, -allColumns[0].transform.localPosition.y - (1.5f * a), allWordsObjects[i].transform.localPosition.z);
                }
                
                for (int j = 1; j < wordsInColumns.Length; j++)
                {
                    if (i + j * wordsInColumns[0] < allWordsObjects.Count)
                    {
                        allWordsObjects[i + j * wordsInColumns[0]].transform.localPosition = new Vector3(allWordsObjects[i].transform.localPosition.x, allWordsObjects[i].transform.localPosition.y, allWordsObjects[i + j * wordsInColumns[0]].transform.localPosition.z);
                    }
                }
            }
        }
    }

    int[] FindAllColumnsWidth(int[] wordsCount, List<string> allSortUsingWords)
    {
        int[] result = new int[wordsCount.Length];
        int startIndex = 0;
        for (int i = 0; i < wordsCount.Length; i++)
        {
            result[i] = FindColomnWidth(wordsCount[i], startIndex, allSortUsingWords);
            startIndex += wordsCount[i];
        }
        return result;
    }

    int FindColomnWidth(int wordsCountOneColomn, int startIndex, List<string> allSortUsingWords)
    {
        int longestWord = 0;
        for (int i = startIndex; i < startIndex + wordsCountOneColomn; i++)
        {
            if (longestWord < allSortUsingWords[i].Length)
            {
                longestWord = allSortUsingWords[i].Length;
            }
        }
        return longestWord;
    }

    float [] FindLongestWordsInColumnLength(int[] longestWordsLettersCount, float letterSize, float gapSize)
    {
        float[] longestWordLength = new float[longestWordsLettersCount.Length];
        for (int i = 0; i < longestWordsLettersCount.Length; i++)
        {
            longestWordLength[i] = longestWordsLettersCount[i] * letterSize + (longestWordsLettersCount[i] - 1) * gapSize;
        }
        return longestWordLength;
    }

    void SetWordsInOneColumnForSeveralColumns(float gapSize, float letterSize, List<string> allSortUsingWords, int wordsInColumn, int startIndex, float verticalPosition, List<GameObject> allWords, List<GameObject> allColumns, float letterScale, List<WordController> allWordControllers)
    {
        GameObject column = Instantiate(columnPrefab, this.gameObject.transform, false) as GameObject;
        allColumns.Add(column);
        for (int i = startIndex; i < startIndex + wordsInColumn; i++)
        {
            float letterPosition = 0f;
            GameObject newWord = CreateWord(verticalPosition, allSortUsingWords, i);
            allWords.Add(newWord);
            allWordsControllers.Add(newWord.GetComponent<WordController>());
            newWord.transform.parent = column.transform;
            for (int j = 0; j < allSortUsingWords[i].Length; j++)
            {
                CreateLetter(newWord, ref letterPosition, letterSize, gapSize, letterScale, allSortUsingWords, i, j);
            }
            verticalPosition -= (letterSize + gapSize) / 100f;
        }
    }

    void SetColumnsPositionsHorizontal(List<GameObject> allColumns, float letterSize, float[] columnsWidth)
    {
        float maxHorizontalPosition = fieldWidth / 2f - letterSize / 2f;
        float position = -(maxHorizontalPosition - minGap);
        allColumns[0].transform.localPosition = new Vector3(position / 100f, allColumns[0].transform.localPosition.y, allColumns[0].transform.localPosition.z);
        for (int i = 1; i < allColumns.Count; i++)
        {
            position += 60f + (columnsWidth[i - 1]);
            allColumns[i].transform.localPosition = new Vector3(position / 100f, allColumns[i].transform.localPosition.y, allColumns[i].transform.localPosition.z);
        }
    }

    void RemoveOverMaxVerticalGapsSeveralColumns(GameObject firstWord, List<GameObject> allWords, float letterSize, GameObject column, int[] wordsInColumns)
    {
        float maxVerticalPosition = fieldHeight / 2f - letterSize / 2f;
        if (firstWord.transform.localPosition.y + column.transform.localPosition.y < (maxVerticalPosition - maxGap) / 100f)
        {
            float position = (maxVerticalPosition - maxGap) / 100f - column.transform.localPosition.y;
            float step = ((maxVerticalPosition - maxGap) / 100f) * 2 / (wordsInColumns[0] - 1);
            for (int i = 0; i < wordsInColumns[0]; i++)
            {
                allWords[i].transform.localPosition = new Vector3(allWords[i].transform.localPosition.x, position - (step * i), allWords[i].transform.localPosition.z);
                int num = i;
                int columnNum = 0;
                for (int j = 1; j < wordsInColumns.Length; j++)
                {                    
                    num += wordsInColumns[j - 1];
                    columnNum += wordsInColumns[j];
                    if (num < wordsInColumns[0] + columnNum)
                    {
                        //string w1 = allWords[num].GetComponent<WordController>().Word;
                        allWords[num].transform.localPosition = new Vector3(allWords[i].transform.localPosition.x, position - (step * i), allWords[i].transform.localPosition.z);                        
                    }                    
                }
            }
        }
    }

    void RemoveDifferenceInColumnsGaps(List<GameObject> allColumns, float letterSize, float[] columnsWidth)
    {
        float minHorizontalPosition = -(fieldWidth / 2f - letterSize / 2f);
        float maxHorizontalPosition = (fieldWidth / 2f + letterSize / 2f) - columnsWidth[columnsWidth.Length - 1];
        float gapFirst = maxHorizontalPosition / 100f - allColumns[allColumns.Count - 1].transform.localPosition.x;
        float gapLast = minHorizontalPosition / 100f - allColumns[0].transform.localPosition.x;
        float gapDifference = (gapFirst + gapLast) / 2f;
        for (int i = 0; i < allColumns.Count; i++)
        {
            allColumns[i].transform.localPosition = new Vector3(allColumns[i].transform.localPosition.x + gapDifference, allColumns[i].transform.localPosition.y, allColumns[i].transform.localPosition.z);
        }
    }

    void RemoveOverMaxHorizontalGapsSeveralColumns(List<GameObject> allColumns, float letterSize, float[] columnsWidth, int columnsCount)
    {
        float minHorizontalPosition = -(fieldWidth / 2f - letterSize / 2f);
        float maxHorizontalPosition = (fieldWidth / 2f + letterSize / 2f) - columnsWidth[columnsWidth.Length - 1];
        if (Mathf.Abs(allColumns[0].transform.localPosition.x) < Mathf.Abs((minHorizontalPosition + maxHorizontalGap) / 100f) || allColumns[allColumns.Count - 1].transform.localPosition.x < (maxHorizontalPosition - maxHorizontalGap) / 100f)
        {
            allColumns[0].transform.localPosition = new Vector3((minHorizontalPosition + maxHorizontalGap) / 100f, allColumns[0].transform.localPosition.y, allColumns[0].transform.localPosition.z);
            allColumns[allColumns.Count - 1].transform.localPosition = new Vector3((maxHorizontalPosition - maxHorizontalGap) / 100f, allColumns[allColumns.Count - 1].transform.localPosition.y, allColumns[allColumns.Count - 1].transform.localPosition.z);
        }
    }

    GameObject CreateWord(float verticalPosition, List<string> allSortUsingWords, int i)
    {
        GameObject newWord = Instantiate(wordPrefab, this.gameObject.transform, false) as GameObject;
        newWord.transform.localPosition = new Vector3(0f, verticalPosition, 0f);
        newWord.GetComponent<WordController>().Word = allSortUsingWords[i];
        return newWord;
    }

    void CreateLetter(GameObject newWord, ref float letterPosition, float letterSize, float gapSize, float letterScale, List<string> allSortUsingWords, int i, int j)
    {
        GameObject letter = Instantiate(lettersPrefab, newWord.transform, false) as GameObject;
        letter.transform.localPosition = new Vector3(letterPosition / 100f, 0f, 0f);
        letterPosition = letterPosition + letterSize + gapSize;
        letter.transform.localScale = new Vector3(letterScale, letterScale, letterScale);
        letter.GetComponent<LetterOnFieldController>().Letter = allSortUsingWords[i][j];
        letter.transform.FindChild("LetterText").gameObject.GetComponent<TextMesh>().text = allSortUsingWords[i][j].ToString().ToUpper();
    }

    public void EndLevel()
    {
        allWordsObjects.Clear();
        allSortWords.Clear();
        allWordsControllers.Clear();
        DestroyFieldObjects("Column");
        DestroyFieldObjects("Word");
    }

    void DestroyFieldObjects(string tag)
    {
        GameObject[] allObjects = GameObject.FindGameObjectsWithTag(tag);
        for (int i = 0; i < allObjects.Length; i++)
        {
            Destroy(allObjects[i].gameObject);
        }
    }
}
