﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public float defaultRatio;
    public float defaultRationToChange;
    public GameObject endLevelPanel;
    public GameObject extraWordsPanel;
    public GameObject pauseMenuPanel;
    public Slider extraWordsSlider;
    public Text extraWordsText;
    public Text coinsText;
    public Text levelCompletePointsText;
    public Text alrearyCompletedText;
    public Text headerText;
    public Button closeExtraWordsButton;
    public Button claimExtraWordsButton;
    public Button nextLevelCompletedButton;
    public Button closeLevelCopmletedButton;
    public GameObject coinsEndPack;
    public RectTransform coinForAnimationImage;
    public RectTransform endCoinPosition;
    public Transform coinsImage;
    public Image hintButton;
    public Image levelCompleteImage;
    public Sprite hintDiscountImage;
    public Sprite hintFullPriceImage;
    public Sprite levelCompleteSprite;
    public Sprite levelCompleteMoreLevelsSprite;
    public Text discountTimeLeftText;
    public GameObject[] scaleObjects;
    public float percentScale;
    public float epsilon;
    public float pointsAddTime;

    float aspectRatio;
    float ratio;
    float finalRatio;
    bool isWindowOpen;
    bool isLastAnimation;

    AnswerFieldController answerFieldContainer;
    AnimationController animationController;
    CoinsController coinsController;
    ScreenCapture screenCapture;

    public float FinalRatio
    {
        get { return finalRatio; }
        set { finalRatio = value; }
    }

    public float AspectRatio
    {
        get { return aspectRatio; }
        set { aspectRatio = value; }
    }

    public float Ratio
    {
        get { return ratio; }
        set { ratio = value; }
    }

    public bool IsWindowOpen
    {
        get { return isWindowOpen; }
        set { isWindowOpen = value; }
    }

    void Awake()
    {
        answerFieldContainer = FindObjectOfType<AnswerFieldController>();
        animationController = FindObjectOfType<AnimationController>();
        coinsController = FindObjectOfType<CoinsController>();
        screenCapture = FindObjectOfType<ScreenCapture>();
        Scale();
    }

    void Start()
    {
        HideEndLevelPanel();
        coinsText.text = PlayerPrefs.GetInt("CoinsCount").ToString();
    }

    public void ShowEndLevelPanel(int points, int levelNumber, int lastLevelInPack, bool isEnd)
    {
        endLevelPanel.SetActive(true);
        closeLevelCopmletedButton.gameObject.SetActive(false);
        nextLevelCompletedButton.gameObject.SetActive(false);
        isWindowOpen = true;        
        levelCompletePointsText.text = points + " points";
        alrearyCompletedText.text = "Already Completed";
        if (isEnd)
        {
            Invoke("ShowLastLevelComplete", 1f);
        }
        else
        {
            levelCompleteImage.sprite = levelCompleteSprite;
            alrearyCompletedText.gameObject.SetActive(true);
            levelCompletePointsText.gameObject.SetActive(true);
            closeLevelCopmletedButton.gameObject.SetActive(true);
            nextLevelCompletedButton.gameObject.SetActive(true);
            coinsEndPack.SetActive(false);
        }
    }

    public void ShowEndPanelWithPoints(int startPoints, int endPoints, int levelNumber, int lastLevelInPack, bool isEnd)
    {
        endLevelPanel.SetActive(true);
        closeLevelCopmletedButton.gameObject.SetActive(false);
        nextLevelCompletedButton.gameObject.SetActive(false);
        isWindowOpen = true;
        levelCompletePointsText.gameObject.SetActive(true);
        alrearyCompletedText.text = "Congratulations!";
        StartCoroutine(ChangePointText(startPoints, endPoints, levelNumber, lastLevelInPack, isEnd));
    }

    IEnumerator ChangePointText(int startPoints, int endPoints, int levelNumber, int lastLevelInPack, bool isEnd)
    {
        int currentPoints = startPoints;
        while (currentPoints <= endPoints)
        {
            levelCompletePointsText.text = currentPoints + " points";
            currentPoints++;
            yield return new WaitForSeconds(pointsAddTime / (endPoints - startPoints));
        }
        if (isEnd)
        {
            Invoke("ShowLastLevelComplete", 1f);
        }
        else
        {
            levelCompleteImage.sprite = levelCompleteSprite;
            alrearyCompletedText.gameObject.SetActive(true);
            coinsEndPack.SetActive(false);
            levelCompletePointsText.gameObject.SetActive(true);
            if (levelNumber == lastLevelInPack)
            {
                isLastAnimation = true;
                coinForAnimationImage.gameObject.SetActive(true);
                coinsImage.SetAsLastSibling();
                coinForAnimationImage.SetAsLastSibling();
                alrearyCompletedText.gameObject.SetActive(false);
                coinsEndPack.SetActive(true);
                Invoke("AddCoins", animationController.coinAnimationTime);
                animationController.AnimateCoin(coinForAnimationImage, coinForAnimationImage.position, endCoinPosition.position);

            }
            else
            {
                closeLevelCopmletedButton.gameObject.SetActive(true);
                nextLevelCompletedButton.gameObject.SetActive(true);
            }
        }
    }

    void AddCoins()
    {
        coinsController.ChangeCoinsValue(50);
        closeLevelCopmletedButton.gameObject.SetActive(true);
        nextLevelCompletedButton.gameObject.SetActive(true);
    }

    void ShowLastLevelComplete()
    {
        alrearyCompletedText.gameObject.SetActive(false);
        levelCompletePointsText.gameObject.SetActive(false);
        closeLevelCopmletedButton.gameObject.SetActive(true);
        nextLevelCompletedButton.gameObject.SetActive(true);
        levelCompleteImage.sprite = levelCompleteMoreLevelsSprite;
    }

    public void HideEndLevelPanel()
    {
        endLevelPanel.SetActive(false);
        isWindowOpen = false;
    }

    public void Scale()
    {
        aspectRatio = (Screen.height * 1f) / (Screen.width * 1f);
        
        if (Mathf.Abs(defaultRatio - aspectRatio) < epsilon)
        {
            return;
        }
        else
        {
            ratio = (defaultRatio / defaultRationToChange) / (defaultRatio / aspectRatio);
            finalRatio = percentScale / ratio;
            answerFieldContainer.fieldWidth = answerFieldContainer.fieldWidth * finalRatio;
        }
        for (int i = 0; i < scaleObjects.Length; i++)
        {
            RectTransform rectTransform = scaleObjects[i].GetComponent<RectTransform>();
            scaleObjects[i].GetComponent<RectTransform>().localScale = new Vector3(rectTransform.localScale.x * finalRatio, rectTransform.localScale.y, rectTransform.localScale.z);
        }
    }

    public void OpenExtraWordScreen()
    {

        extraWordsPanel.SetActive(true);
        isWindowOpen = true;
    }

    public void CloseExtraWordScreen()
    {
        extraWordsPanel.SetActive(false);
        isWindowOpen = false;
    }

    public void SetExtraWordsValue(int wordsCount, int maxWords)
    {
        extraWordsSlider.value = wordsCount;
        extraWordsSlider.maxValue = maxWords;
        extraWordsText.text = wordsCount + "/" + maxWords;
    }

    public void MakeReadyToClaim()
    {
        closeExtraWordsButton.gameObject.SetActive(false);
        claimExtraWordsButton.gameObject.SetActive(true);
    }

    public void Claim()
    {
        closeExtraWordsButton.gameObject.SetActive(true);
        claimExtraWordsButton.gameObject.SetActive(false);
    }

    public void ShowPauseMenu()
    {
        StartCoroutine(MakeScreenShot());
    }

    IEnumerator MakeScreenShot()
    {
        screenCapture.SaveScreenshot(CaptureMethod.ReadPixels_Synch, Application.persistentDataPath + "/AskFriends.png");
        yield return new WaitForEndOfFrame();
        pauseMenuPanel.SetActive(true);
        isWindowOpen = true;
    }

    public void HidePauseMenu()
    {
        pauseMenuPanel.SetActive(false);
        isWindowOpen = false;
    }

    public void ChangeCoinsText(int value)
    {
        coinsText.text = value.ToString();
    }

    public void OpenCoinWindow()
    {
        if (!isLastAnimation)
        {
            isWindowOpen = true;
            coinsController.OpenShop();
        }
    }

    public void CloseCoinWindow()
    {
        isWindowOpen = false;
        coinsController.CloseShop();
    }

    public void GetCoins(int coinsCount)
    {
        //isWindowOpen = false;
        coinsController.OnShopButtonClick(coinsCount);
    }

    public void SetHeaderName(string name, int number)
    {
        headerText.text = name + " " + number;
    }

    public void StartDiscount()
    {
        hintButton.sprite = hintDiscountImage;
        discountTimeLeftText.gameObject.SetActive(true);
    }

    public void StopDiscount()
    {
        hintButton.sprite = hintFullPriceImage;
        discountTimeLeftText.gameObject.SetActive(false);
    }

    public void UpdateDiscount(float timeLeft)
    {
        int minutes;
        int seconds;
        string niceTime;
        minutes = Mathf.FloorToInt(timeLeft / 60F);
        seconds = Mathf.FloorToInt(timeLeft - minutes * 60);
        niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);
        discountTimeLeftText.text = niceTime;
    }
}
