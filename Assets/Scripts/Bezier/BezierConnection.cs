﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BezierConnection : MonoBehaviour
{
    bool isBezierStarted;
    int lettersAdded;
    public List<LetterBezierController> connectedLetters = new List<LetterBezierController>();
    public GameObject wordProgressObject;
    public GameObject letterProgressPrefab;
    GameController gameController;
    List<GameObject> allLettersProgress = new List<GameObject>();

    Color defaultWordObjectColor;

    void Start()
    {
        gameController = FindObjectOfType<GameController>();
        defaultWordObjectColor = wordProgressObject.GetComponent<Image>().color;
    }

    public bool IsBezierStarted
    {
        get { return isBezierStarted; }
        set { isBezierStarted = value; }
    }
    public int LettersAdded
    {
        get { return lettersAdded; }
        set { lettersAdded = value; }
    }
    public List<LetterBezierController> ConnectedLetters
    {
        get { return connectedLetters; }
    }

    public void StartBezier(LetterBezierController letter)
    {
        ClearProgressLetters();
        CancelInvoke("ClearProgressLetters");
        isBezierStarted = true;
        lettersAdded++;
        connectedLetters.Add(letter);
        AddProgressLetter(lettersAdded - 1, letter);
    }

    public void AddBezier(LetterBezierController letter)
    {
        lettersAdded++;
        connectedLetters.Add(letter);
        AddProgressLetter(lettersAdded - 1, letter);
    }

    public void RemoveBezier(LetterBezierController letter)
    {
        lettersAdded--;
        connectedLetters.Remove(letter);
        RemoveProgressLetter(lettersAdded);
    }

    public void StopBezier()
    {
        gameController.CheckChosenWord(connectedLetters);
        isBezierStarted = false;
        lettersAdded = 0;
        connectedLetters.Clear();
        Invoke("ClearProgressLetters", 0.5f);
    }

    public void CreateProgressLetter(int lettersCount)
    {
        for (int i = 0; i < lettersCount; i++)
        {
            GameObject letter = Instantiate(letterProgressPrefab, wordProgressObject.transform);
            allLettersProgress.Add(letter);
            letter.GetComponent<Renderer>().sortingOrder = 3;
        }
    }

    void AddProgressLetter(int number, LetterBezierController letter)
    {
        allLettersProgress[number].SetActive(true);
        allLettersProgress[number].GetComponent<TextMesh>().text = letter.Letter.ToString().ToUpper();
    }

    void RemoveProgressLetter(int number)
    {
        allLettersProgress[number].SetActive(false);
    }

    void ClearProgressLetters()
    {
        for (int i = 0; i < allLettersProgress.Count; i++)
        {
            allLettersProgress[i].SetActive(false);
        }

        wordProgressObject.GetComponent<Image>().color = defaultWordObjectColor;
    }

    public void ChangeWordProgressObjectColor(Color color)
    {
        wordProgressObject.GetComponent<Image>().color = color;
    }
}
