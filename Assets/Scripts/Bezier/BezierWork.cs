﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierWork : MonoBehaviour
{
    public List<Vector3> points;
    private List<Vector3> gizmos;
    private LineRenderer lineRenderer;
    BezierConnection bezierConnection;
    UIController uiController;

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.sortingOrder = 5;
        bezierConnection = GetComponent<BezierConnection>();
        uiController = FindObjectOfType<UIController>();
        points = new List<Vector3>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), transform.forward, Mathf.Infinity);
            if (hit && hit.collider.gameObject.tag == "Letter")
            {
                LetterBezierController letter = hit.collider.gameObject.GetComponent<LetterBezierController>();
                if (!bezierConnection.ConnectedLetters.Contains(letter) && !uiController.IsWindowOpen)
                {
                    StartBezier(letter);
                }
                return;
            }
        }

        if (Input.GetMouseButton(0) && bezierConnection.IsBezierStarted)
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), transform.forward, Mathf.Infinity);
            if (!hit)
            {
                if (points.Count > bezierConnection.ConnectedLetters.Count)
                {
                    points.RemoveAt(points.Count - 1);
                }
                AddPoint(ChangeZPosition(GetMouseWorldPosition()));
                BezierInterpolate();
                return;
            }
            else if (hit && hit.collider.gameObject.tag == "Letter")
            {
                LetterBezierController letter = hit.collider.gameObject.GetComponent<LetterBezierController>();
                if (!bezierConnection.ConnectedLetters.Contains(letter))
                {
                    points.RemoveAt(points.Count - 1);
                    AddPoint(ChangeZPosition(letter.gameObject.transform.position));
                    bezierConnection.AddBezier(letter);
                    BezierInterpolate();
                }
                if (bezierConnection.LettersAdded > 1)
                {
                    if (letter.Equals(bezierConnection.ConnectedLetters[bezierConnection.connectedLetters.Count - 2]))
                    {
                        points.RemoveAt(points.Count - 1);
                        points.Remove(bezierConnection.ConnectedLetters[bezierConnection.connectedLetters.Count - 1].gameObject.transform.position);
                        bezierConnection.RemoveBezier(bezierConnection.ConnectedLetters[bezierConnection.connectedLetters.Count - 1]);
                        BezierInterpolate();
                    }
                }
                return;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            points.Clear();
            BezierInterpolate();
            bezierConnection.StopBezier();
        }
    }

    Vector3 ChangeZPosition(Vector3 startPosition)
    {
        Vector3 endPosition = new Vector3(startPosition.x, startPosition.y, 0f);
        return endPosition;
    }

    public void StartBezier(LetterBezierController letter)
    {
        AddPoint(ChangeZPosition(letter.gameObject.transform.position));
        bezierConnection.StartBezier(letter);
        BezierInterpolate();
    }

    Vector3 GetMouseWorldPosition()
    {
        Vector2 screenPosition = Input.mousePosition;
        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(new Vector3(screenPosition.x, screenPosition.y, 2));
        return worldPosition;
    }

    void AddPoint(Vector3 position)
    {
        points.Add(position);
    }

    void RemovePoint ()
    {

    }

    private void BezierInterpolate()
    {
        BezierPath bezierPath = new BezierPath();
        bezierPath.Interpolate(points, 0.25f);

        List<Vector3> drawingPoints = bezierPath.GetDrawingPoints();

        gizmos = bezierPath.GetControlPoints();

        SetLinePoints(drawingPoints);
    }

    private void SetLinePoints(List<Vector3> drawingPoints)
    {
        lineRenderer.positionCount = drawingPoints.Count;

        for (int i = 0; i < drawingPoints.Count; i++)
        {
            lineRenderer.SetPosition(i, drawingPoints[i]);
        }
    }

    public void OnDrawGizmos()
    {
        if (gizmos == null)
        {
            return;
        }

        for (int i = 0; i < gizmos.Count; i++)
        {
            Gizmos.DrawWireSphere(gizmos[i], 1f);
        }
    }
}
