﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterBezierController : MonoBehaviour
{
    char letter;

    public char Letter
    {
        get { return letter; }
        set { letter = value; }
    }
}
