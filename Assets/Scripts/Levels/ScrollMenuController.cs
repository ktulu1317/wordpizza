﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using WyrmTale;
using UnityEngine.SceneManagement;

public class ScrollMenuController : MonoBehaviour
{
    public ScrollRect scroll;
    public float backTime;
    public int packsGroupsCount;
    public string[] packsGroupsNames;
    public int[] groupsLengths;
    public GameObject packsGroupsParent;
    public GameObject packsGroupPrefab;
    public GameObject startPackInGroupPrefab;
    public GameObject middlePackInGroupPrefab;
    public GameObject lastPackInGroupPrefab;
    JSON packsNames;

    public static bool isEndOfPackLoad;
    public AudioSource sound;

    void Start()
    {
        GetPackFromFile();
        if (isEndOfPackLoad)
        {
            isEndOfPackLoad = false;
            ChoosePack(GameController.packNumber);
        }
        else
        {
            GeneratePackChooseMenu();
        }
    }

    void LateUpdate()
    {
        scroll.verticalNormalizedPosition = Mathf.Clamp01(scroll.verticalNormalizedPosition);
    }

    void GetPackFromFile()
    {
        TextAsset packsNamesFile = Resources.Load("PacksNames") as TextAsset;
        packsNames = new JSON();
        packsNames.serialized = packsNamesFile.ToString();
    }

    void GeneratePackChooseMenu()
    {
        int lastLevelInPack = 0;
        int currentLevelID = -1;
        GameObject firstPackInGroup = null;
        for (int i = 0; i < packsGroupsCount; i++)
        {
            GameObject currentGroup = Instantiate(packsGroupPrefab, packsGroupsParent.transform) as GameObject;
            for (int j = 0; j < groupsLengths[i]; j++)
            {
                currentLevelID++;
                lastLevelInPack += packsNames.ToArray<JSON>("names")[currentLevelID].ToInt("length");
                GameObject currentPack;
                if (j == 0)
                {
                    currentPack = Instantiate(startPackInGroupPrefab, currentGroup.transform) as GameObject;
                    Text headerText = currentPack.transform.FindChild("Header/HeaderText").gameObject.GetComponent<Text>();
                    headerText.text = packsGroupsNames[i];
                    firstPackInGroup = currentPack;
                }
                else if (j == groupsLengths[i] - 1)
                {
                    currentPack = Instantiate(lastPackInGroupPrefab, currentGroup.transform) as GameObject;
                    if (lastLevelInPack <= PlayerPrefs.GetInt("LevelsFinished"))
                    {
                        firstPackInGroup.transform.FindChild("Field/Cup").gameObject.SetActive(true);
                    }
                }
                else
                {
                    currentPack = Instantiate(middlePackInGroupPrefab, currentGroup.transform) as GameObject;
                }

                if (lastLevelInPack <= PlayerPrefs.GetInt("LevelsFinished"))
                {
                    currentPack.transform.FindChild("Field/Crown").gameObject.SetActive(true);
                }
                GetPackName(packsNames, currentPack, i + 1, j + 1);
            }
        }
    }

    void GetPackName(JSON packsName, GameObject currentPack, int groupId, int packId)
    {
        for (int i = 0; i < packsName.ToArray<JSON>("names").Length; i++)
        {
            if (packsName.ToArray<JSON>("names")[i].ToInt("idGroup") == groupId && packsName.ToArray<JSON>("names")[i].ToInt("idPack") == packId)
            {
                Text packName = currentPack.transform.FindChild("Field/PackNameText").gameObject.GetComponent<Text>();
                packName.text = (packsName.ToArray<JSON>("names")[i].ToString("Name"));

                int maxLetters = packsName.ToArray<JSON>("names")[i].ToInt("MaxLetters");
                Text packLettersText = currentPack.transform.FindChild("Field/MaxLettersText").gameObject.GetComponent<Text>();
                packLettersText.text = maxLetters + " cookies max";

                int maxPoints = (4 + packsName.ToArray<JSON>("names")[i].ToInt("id")) * packsNames.ToArray<JSON>("names")[i].ToInt("length");
                Text pointsText = currentPack.transform.FindChild("Field/Points/Text").gameObject.GetComponent<Text>();
                pointsText.text = maxPoints.ToString();

                Button b = currentPack.transform.FindChild("Field").gameObject.GetComponent<Button>();
                int levelNumber = packsName.ToArray<JSON>("names")[i].ToInt("id");
                int startLevel = 0;
                for (int j = 0; j < levelNumber - 1; j++)
                {
                    startLevel += packsNames.ToArray<JSON>("names")[j].ToInt("length");
                }
                if (startLevel > PlayerPrefs.GetInt("LevelsFinished"))
                {
                    b.interactable = false;
                }
                b.onClick.AddListener(delegate { ChoosePack(levelNumber); });
            }
        }
    }

    public void ChoosePack(int param)
    {
        sound.Play();
        int startLevel = 1;
        for (int i = 0; i < param - 1; i++)
        {
            startLevel += packsNames.ToArray<JSON>("names")[i].ToInt("length");
        }
        LevelChooseController.startLevel = startLevel;
        LevelChooseController.packLength = packsNames.ToArray<JSON>("names")[param - 1].ToInt("length");
        LevelChooseController.packHeaderName = packsNames.ToArray<JSON>("names")[param - 1].ToString("Name");
        GameController.packNumber = packsNames.ToArray<JSON>("names")[param - 1].ToInt("id");
        SceneManager.LoadScene(2);
    }

    public void Back()
    {
        SceneManager.LoadScene(0);
    }
}
