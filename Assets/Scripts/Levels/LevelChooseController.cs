﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class LevelChooseController : MonoBehaviour
{
    public static int startLevel;
    public static int packLength;
    public static string packHeaderName;

    public ScrollRect scroll;
    public float backTime;
    public GameObject packsGroupsParent;
    public GameObject packsGroupPrefab;
    public GameObject startPackInGroupPrefab;
    public GameObject middlePackInGroupPrefab;
    public GameObject lastPackInGroupPrefab;
    public RectTransform content;
    public AudioSource soundSource;

    bool isStart;

    void Start ()
    {
        GeneratePackChooseMenu();
        content.anchoredPosition = new Vector2(content.anchoredPosition.x, 0f);
        Invoke("MakeNull", 0.1f);
    }

    void LateUpdate()
    {
        if (isStart)
        {
            scroll.verticalNormalizedPosition = Mathf.Clamp01(scroll.verticalNormalizedPosition);
        }    
    }

    void MakeNull()
    {
        content.anchoredPosition = new Vector2(content.anchoredPosition.x, 0f);
        isStart = true;
    }

    void GeneratePackChooseMenu()
    {
        GameObject currentGroup = Instantiate(packsGroupPrefab, packsGroupsParent.transform) as GameObject;
        for (int j = 0; j < packLength; j++)
        {
            GameObject currentPack;
            if (j == 0)
            {
                currentPack = Instantiate(startPackInGroupPrefab, currentGroup.transform) as GameObject;
                Text headerText = currentPack.transform.FindChild("Header/HeaderText").gameObject.GetComponent<Text>();
                headerText.text = packHeaderName;
            }
            else if (j == packLength - 1)
            {
                currentPack = Instantiate(lastPackInGroupPrefab, currentGroup.transform) as GameObject;
            }
            else
            {
                currentPack = Instantiate(middlePackInGroupPrefab, currentGroup.transform) as GameObject;
            }
            int levelNumber = j + 1;
            Text packName = currentPack.transform.FindChild("Field/PackNameText").gameObject.GetComponent<Text>();
            packName.text = "Level " + levelNumber;

            Text packLettersText = currentPack.transform.FindChild("Field/MaxLettersText").gameObject.GetComponent<Text>();

            currentPack.transform.FindChild("Field/Points").gameObject.SetActive(false);
            if (PlayerPrefs.GetInt("LevelsFinished") >= startLevel + j)
            {
                packLettersText.text = "Solved";
            }
            else
            {
                packLettersText.text = "Not solved";
            }

            Button b = currentPack.transform.FindChild("Field").gameObject.GetComponent<Button>();
            if (PlayerPrefs.GetInt("LevelsFinished") + 1 >= startLevel + j)
            {
                b.interactable = true;
            }
            else
            {
                b.interactable = false;
            }
            b.onClick.AddListener(delegate { ChooseLevel(levelNumber); });
        }
    }

    public void ChooseLevel(int param)
    {
        soundSource.Play();
        GameController.levelNumber = startLevel + param - 1;
        GameController.lastLevelInPack = startLevel + packLength - 1;
        GameController.packName = packHeaderName;
        GameController.numberInPack = param;
        if (PlayerPrefs.GetInt("IsRate") == 0 && GameController.levelNumber > 1)
        {  
            if (!RatePopup.ShowPopup()) {
                SceneManager.LoadScene(3);
            }   
        }
        else
        {
            SceneManager.LoadScene(3);
        }
            
    }

    public void Back()
    {
        SceneManager.LoadScene(1);
    }
}
