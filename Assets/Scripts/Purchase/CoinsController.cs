﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsController : MonoBehaviour
{
    public static CoinsController instance;
    public static int shopClosingNumber;
    public int startCoinsValue;
    public GameObject coinsShopMenu;
    public GameObject restoreMessage;
    public GameObject restoreButton;
    int coinsCount;
    bool isPurchased = false;
    bool isCorrectPlatform;
    public Text coinsText;
    public Text restoreText;
    public Text[] itemPriceCaption;

    public int CoinsCount
    {
        get { return coinsCount; }
        set { coinsCount = value; }
    }

    public bool IsPurchased
    {
        get { return isPurchased; }
        set { isPurchased = value; }
    }

    public bool IsCorrectPlatform
    {
        get { return isCorrectPlatform; }
        set { isCorrectPlatform = value; }
    }

    void Awake()
    {
        //PlayerPrefs.SetInt("CoinsCount", 0);
        //PlayerPrefs.DeleteKey("CoinsCount");
        if (!PlayerPrefs.HasKey("CoinsCount"))
        {
            PlayerPrefs.SetInt("CoinsCount", startCoinsValue);
        }
        coinsCount = PlayerPrefs.GetInt("CoinsCount");
        instance = this;
    }

    void Start()
    {
        //coinsShopMenu = GameObject.FindGameObjectWithTag("CoinsShop");
        coinsText.text = PlayerPrefs.GetInt("CoinsCount").ToString();
        DisableNoAdsButton();
        DisableRestoreButton();
    }

    public void DisableNoAdsButton()
    {
        GameObject noAdsButton = coinsShopMenu.transform.Find("ShopWindow/Content/ShopElement (5)").gameObject;
        if (PlayerPrefs.GetInt("NoAds") == 1)
        {
            noAdsButton.SetActive(false);
        }
        else
        {
            noAdsButton.SetActive(true);
        }
    }

    public void DisableRestoreButton()
    {
#if UNITY_ANDROID
        restoreButton.SetActive(false);
#endif
    }

    public void OnShopButtonClick(int id)
    {
        switch (id)
        {
            case 1:
                PurchasesController.instance.BuyProduct(purchaseType.coinsPack01);
                break;
            case 2:
                PurchasesController.instance.BuyProduct(purchaseType.coinsPack02);
                break;
            case 3:
                PurchasesController.instance.BuyProduct(purchaseType.coinsPack03);
                break;
            case 4:
                PurchasesController.instance.BuyProduct(purchaseType.coinsPack04);
                break;
            case 5:
                PurchasesController.instance.BuyProduct(purchaseType.coinsPack05);
                break;
            case 6:
                PurchasesController.instance.BuyProduct(purchaseType.noAds);
                break;
        }
    }

    public void OnRestoreButtonClick()
    {
        PurchasesController.instance.RestorePurchases();
    }

    public void ChangeCoinsValue(int changeNumber)
    {
        if (coinsCount + changeNumber >= 0)
        {
            coinsCount += changeNumber;
            PlayerPrefs.SetInt("CoinsCount", coinsCount);
            coinsText.text = coinsCount.ToString();


            //CloseShop();
        }
    }

    public void OpenShop()
    {
        AdsController.instance.isShopShown = true;
        AdsController.instance.showShopAd = true;
        coinsShopMenu.SetActive(true);
        //m_StoreController.products.WithID("pack_gold1").metadata.localizedDescription;
        itemPriceCaption[0].text = PurchasesController.instance.GetLocalizedPrice(purchaseType.coinsPack01);
        itemPriceCaption[1].text = PurchasesController.instance.GetLocalizedPrice(purchaseType.coinsPack02);
        itemPriceCaption[2].text = PurchasesController.instance.GetLocalizedPrice(purchaseType.coinsPack03);
        itemPriceCaption[3].text = PurchasesController.instance.GetLocalizedPrice(purchaseType.coinsPack04);
        itemPriceCaption[4].text = PurchasesController.instance.GetLocalizedPrice(purchaseType.coinsPack05);
        itemPriceCaption[5].text = PurchasesController.instance.GetLocalizedPrice(purchaseType.noAds);

        /*Debug.Log(PurchasesController.instance.GetLocalizedPrice(purchaseType.coinsPack01));
		Debug.Log(PurchasesController.instance.GetLocalizedPrice(purchaseType.coinsPack02));
		Debug.Log(PurchasesController.instance.GetLocalizedPrice(purchaseType.coinsPack03));
		Debug.Log(PurchasesController.instance.GetLocalizedPrice(purchaseType.coinsPack04));
		Debug.Log(PurchasesController.instance.GetLocalizedPrice(purchaseType.coinsPack05));
		Debug.Log(PurchasesController.instance.GetLocalizedPrice(purchaseType.noAds));*/
    }

    public void CloseShop()
    {
        shopClosingNumber++;
        if (AdsController.instance.InAppCloseCount == 0)
        {
            AdsController.instance.InAppCloseCount = 1;
        }
        if ((AdsController.instance != null) && (AdsController.instance.showShopAd) && shopClosingNumber % AdsController.instance.InAppCloseCount == 0)
        {
            if (AdsController.instance.CancelScreenAdType == 1)
            {
                AdsController.instance.ShowInterstitial(interstitialType.interstitialRefuseInApp);
            }
            else if (AdsController.instance.CancelScreenAdType == 2)
            {
                AdsController.instance.ShowAppLovinInterstitial();
            }
            else if (AdsController.instance.CancelScreenAdType == 3)
            {
                //FACEBOOK AD SHOW
            }
        }
        coinsShopMenu.SetActive(false);
        AdsController.instance.isShopShown = false;
    }

    public void CloseRestoreMessage()
    {
        restoreMessage.SetActive(false);
    }

    public void CheckRectore()
    {
        restoreMessage.SetActive(true);
        if (isPurchased && isCorrectPlatform)
        {
            restoreText.text = "Restore successful";
        }
        else if (!isPurchased && isCorrectPlatform)
        {
            restoreText.text = "No purchases available to restore";
        }
        else if (!IsCorrectPlatform)
        {
            restoreText.text = "Not supported on this platform";
        }
        isPurchased = false;
        isCorrectPlatform = false;
    }
}
