﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using UnityEngine;
using System.Security.Cryptography;

public class Crypt : MonoBehaviour
{
    public static Crypt instance;

    void Awake()
    {
        instance = this;
    }

    static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
    {
        // Check arguments.
        if (plainText == null || plainText.Length <= 0)
            Debug.LogError("plainText");
        if (Key == null || Key.Length <= 0)
            Debug.LogError("Key");
        if (IV == null || IV.Length <= 0)
            Debug.LogError("IV");
        byte[] encrypted;
        // Create an Aes object
        // with the specified key and IV.
        using (Aes aesAlg = Aes.Create())
        {
            aesAlg.Key = Key;
            aesAlg.IV = IV;
            aesAlg.Mode = CipherMode.ECB;
            // Create a decrytor to perform the stream transform.
            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for encryption.
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        //Write all data to the stream.
                        swEncrypt.Write(plainText);
                    }
                    encrypted = msEncrypt.ToArray();
                    string e = Encoding.ASCII.GetString(msEncrypt.ToArray());
                }
            }
        }
        // Return the encrypted bytes from the memory stream.
        return encrypted;
    }

    static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
    {
        // Check arguments.
        if (cipherText == null || cipherText.Length <= 0)
            Debug.LogError("cipherText");
        if (Key == null || Key.Length <= 0)
            Debug.LogError("Key");
        if (IV == null || IV.Length <= 0)
            Debug.LogError("IV");

        // Declare the string used to hold
        // the decrypted text.
        string plaintext = null;

        // Create an Aes object
        // with the specified key and IV.
        using (Aes aesAlg = Aes.Create())
        {
            aesAlg.Key = Key;
            aesAlg.IV = IV;
            aesAlg.Mode = CipherMode.ECB;
            // Create a decrytor to perform the stream transform.
            ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for decryption.
            using (MemoryStream msDecrypt = new MemoryStream(cipherText))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {
                        // Read the decrypted bytes from the decrypting 
                        plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }

        }

        return plaintext;
    }

    public string Decrypt(string message)
    {
        string key = "_mysecretmacbook";
        byte[] IVbyte = new byte[16];
        Array.Clear(IVbyte, 0, IVbyte.Length);
        string result = "";
        using (AesManaged myAes = new AesManaged())
        {
            myAes.Mode = CipherMode.ECB;
            myAes.Padding = PaddingMode.PKCS7;
            myAes.Key = Encoding.ASCII.GetBytes(key);
            myAes.IV = IVbyte;
            result = DecryptStringFromBytes_Aes(Convert.FromBase64String(message), myAes.Key, myAes.IV);
        }
        return result;
    }
}